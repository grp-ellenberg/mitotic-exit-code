ID	POI	Cellline	path	ChPOI	Chr_marker	start_slice	end_slice	n_timepoints	processuptoframe	manualQC	method	cell_cycle_stage	bg_subtract	replicate	file_format
1	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell01	1	2	1	5	31	30	1	Default	metaphase	FALSE	1	.czi
2	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell02	1	2	1	5	31	12	1	Default	metaphase	FALSE	1	.czi
3	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
4	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell04	1	2	1	5	31	15	1	Default	metaphase	FALSE	1	.czi
5	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
6	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell06	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
7	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell07	1	2	1	5	31	15	1	Default	metaphase	FALSE	1	.czi
8	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell08	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
9	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R1\NCAPH_cell09	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
10	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
11	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell02	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
12	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell03	1	2	1	5	19	17	1	Default	metaphase	FALSE	2	.czi
13	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
14	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell05	1	2	1	5	20	17	1	Default	metaphase	FALSE	2	.czi
15	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell06	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
16	NCAPH	NCAPH-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH_R2\220816_NCAPH_cell07	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
17	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
18	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell02	1	2	1	5	21	16	1	Default	metaphase	FALSE	1	.czi
19	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell03	1	2	1	5	23	15	1	Default	metaphase	FALSE	1	.czi
20	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
21	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
22	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell06	1	2	1	5	29	26	1	Default	metaphase	FALSE	1	.czi
23	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R1\220824_CTCF_cell07	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
24	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell01	1	2	1	5	22	20	1	Default	metaphase	FALSE	2	.czi
25	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell02	1	2	1	3	20	18	1	Default	metaphase	FALSE	2	.czi
26	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell03	1	2	1	5	16	14	1	Default	metaphase	FALSE	2	.czi
27	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
28	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
29	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell06	1	2	1	5	17	14	1	Default	metaphase	FALSE	2	.czi
30	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell07	1	2	1	5	16	13	1	Default	metaphase	FALSE	2	.czi
31	CTCF	CTCF-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\CTCF_R2\220830_CTCF_cell08	1	2	1	5	31	14	1	Default	metaphase	FALSE	2	.czi
32	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R1\RAD21_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
33	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R1\RAD21_cell02	1	2	1	5	31	29	1	Default	metaphase	FALSE	1	.czi
34	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R1\RAD21_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
35	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R1\RAD21_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
36	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R1\RAD21_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
37	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
38	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell02	1	2	1	5	31	21	1	Default	metaphase	FALSE	2	.czi
39	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
40	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell04	1	2	1	5	31	29	1	Default	metaphase	FALSE	2	.czi
41	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
42	RAD21	RAD21-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\RAD21_R2\220816_RAD21_cell06	1	2	1	5	31	24	1	Default	metaphase	FALSE	2	.czi
43	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
44	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell02	1	2	1	5	30	15	1	Default	metaphase	FALSE	1	.czi
45	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
46	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell04	1	2	1	5	31	28	1	Default	metaphase	FALSE	1	.czi
47	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell05	1	2	1	5	21	17	1	Default	metaphase	FALSE	1	.czi
48	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell06	1	2	1	5	24	21	1	Default	metaphase	FALSE	1	.czi
49	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R1\220824_STAG1_cell07	1	2	1	3	24	22	1	Default	metaphase	FALSE	1	.czi
50	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell01	1	2	1	5	23	19	1	Default	metaphase	FALSE	2	.czi
51	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell02	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
52	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell03	1	2	1	5	16	14	1	Default	metaphase	FALSE	2	.czi
53	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
54	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell05	1	2	1	5	31	27	1	Default	metaphase	FALSE	2	.czi
55	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell06	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
56	STAG1	STAG1-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG1_R2\220831_STAG1_cell07	1	2	1	3	31	31	1	Default	metaphase	FALSE	2	.czi
57	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell01	1	2	1	5	24	22	1	Default	metaphase	FALSE	1	.czi
58	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell02	1	2	1	3	22	19	1	Default	metaphase	FALSE	1	.czi
59	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
60	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
61	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
62	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R1\220830_STAG2_cell06	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
63	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell01	1	2	1	5	31	29	1	Default	metaphase	FALSE	2	.czi
64	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell02	1	2	1	3	31	31	1	Default	metaphase	FALSE	2	.czi
65	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
66	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell04	1	2	1	5	31	22	1	Default	metaphase	FALSE	2	.czi
67	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell05	1	2	1	5	22	16	1	Default	metaphase	FALSE	2	.czi
68	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell06	1	2	1	5	31	26	1	Default	metaphase	FALSE	2	.czi
69	STAG2	STAG2-mEGFP	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\STAG2_R2\220831_STAG2_cell07	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
70	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
71	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell02	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
72	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
73	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
74	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
75	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R1\210119_SMC4_R1_metaphase_cell06	1	2	1	5	23	21	1	Default	metaphase	FALSE	1	.czi
76	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
77	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell02	1	2	1	5	31	15	1	Default	metaphase	FALSE	2	.czi
78	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
79	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell04	1	2	1	5	16	13	1	Default	metaphase	FALSE	2	.czi
80	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
81	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell06	1	2	1	5	19	13	1	Default	metaphase	FALSE	2	.czi
82	SMC4	HK SMC4-mEGFP #82_68	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\SMC4_R2\230126_SMC4_metaphase_R2_cell07	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
83	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
84	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell02	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
85	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell03	1	2	1	5	21	18	1	Default	metaphase	FALSE	1	.czi
86	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell04	1	2	1	5	30	23	1	Default	metaphase	FALSE	1	.czi
87	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell05	1	2	1	5	31	16	1	Default	metaphase	FALSE	1	.czi
88	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell06	1	2	1	5	31	31	1	Default	metaphase	FALSE	1	.czi
89	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R1\230126_NCAPH2_metaphase_R1_cell07	1	2	1	4	31	31	1	Default	metaphase	FALSE	1	.czi
90	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R2\230127_NCAPH2_metaphase_R2_cell01	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
91	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R2\230127_NCAPH2_metaphase_R2_cell02	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
92	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R2\230127_NCAPH2_metaphase_R2_cell03	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
93	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R2\230127_NCAPH2_metaphase_R2_cell04	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
94	NCAPH2	HK NCAPH2-mEGFP #67	P:\Andi\FRAP\220906_FRAP_analysis\01_image_data_unprocessed\metaphase\NCAPH2_R2\230127_NCAPH2_metaphase_R2_cell05	1	2	1	5	31	31	1	Default	metaphase	FALSE	2	.czi
