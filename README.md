# Mitotic-exit-code

This repository contains code for image and data analysis related to [Brunner et al. 2024](https://pubmed.ncbi.nlm.nih.gov/38854067/). <br/>
All code related to this manuscript is deposited here, alongside processed data. <br/>
The raw image data and processed LoopTrace data is available via BioImageArchive under accession number [S-BIAD1454](https://www.ebi.ac.uk/biostudies/bioimages/studies/S-BIAD1454).

---

## 01 Full Cell Cycle Imaging

Code for 
* tracking and segmentation of dividing cells
* the downstream data processing and visualization

and the following processed data:
* output from segmentation pipeline
* Analysis Code for full cell cycle data

## 02 Mitotic Exit Imaging

Code for
* Segmentation pipeline to track dividing cells, segment their landmarks (cell, nucleus) and extract and convert GFP intensities of those.
* the downstream data processing and visualization

and the following processed data: 
* output from segmentation & calibration pipeline
* average and median bound fractions as determined by spot-bleach (for calculation of absolute chromatin-bound N)

## 03 Immunofluorescence of mitotic exit cells

Code for
* Segmentation pipeline of images
* the downstream data processing and visualization

and the following processed data:
* Segmentation output for Figs. 2F&G, 3D&E

## 04 Spot-bleach data

* Example Raw data and all data summaries
* Code for analysis of data summary

## 05 FRAP

Code for
* Image Analysis (Adapted from [Walther et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018))
* Data processing (Adapted from [Walther et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018))
* Recovery analysis, data fitting and plotting

and the following data: 
* database files for analysis of image data
* processed FRAP data (post image analysis and processing, input for Recovery analysis and data fitting)
* data fit results

## 06 LoopTrace
Code related to processed data analysis

## 07 STED
Code related to
* image segmentation
* image analysis & visualization
* spot simulation

## Contributing
* Andreas Brunner
* Julius Hossain
* Kai Sandvold Beckwith
