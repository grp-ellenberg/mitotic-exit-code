# 02 FCS-Calibrated Mitotic Exit Imaging

## Segmentation
The segmentation pipeline employed in Brunner et al. 2024 is based on [Cai et al. 2018](https://www.nature.com/articles/s41586-018-0518-z) and [Cattoglio et al. 2018](https://elifesciences.org/articles/40164) and requires MTALAB.

### Requirements

**Toolboxes:** Computer Vision Toolbox, Curve Fitting Toolbox\
**Image and segmentation parameters** are to be defined in fn_running_options.m:
* image directory, following acquisition folder structure of MyPic
* output directory
* image file extension (.czi or .tif)
* segmentation parameters, especially analyzed cropped region
* experiment specific calibration parameters were determined as described recently in [Politi et al. 2018](https://www.nature.com/articles/nprot.2018.040). 

### Segmented data (.csv files) are provided [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/02_MExit_imaging/01_Extracted_parameters_combined_by_POI?ref_type=heads)

## Analysis

Contains [functions for analysis](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/02_MExit_imaging/02_Analysis/MExit_functions.py?ref_type=heads) and a [script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Code/02_MExit_imaging/02_Analysis?ref_type=heads) for the combined analysis and plotting of the segmented data plotting of FCS-calibrated imaging data

Uses [Spot-bleach photobleaching data](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/02_MExit_imaging/02_spot_bleach_summary?ref_type=heads) to estimate chromatin-bound proteins from metaphase through 2 hours past mitosis.
