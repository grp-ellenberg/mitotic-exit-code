### Functions to analyse the data set: Mitotic Exit FCS-calibrated imaging data
### Author: Andreas Brunner
### Date: 2020-2024

import pandas as pd
import seaborn as sns
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

def number_of_time_series(df):
    """
    Function to extract number of time-series in the given dataframe
    df: DataFrame object with at least one time-series of length 75
    """
    total_rows = len(df)
    n_time_series = int(total_rows/75)
    #print('number of time-series for ' + df.name + ' = ' + str(n_time_series))
    return n_time_series

def plot_time_series(df, what_to_plot):
    """
    Function to plot a variable of interest all time-series in the given dataframe.
    """
    n_time_series = number_of_time_series(df)
    lis = list()
    for element in range(n_time_series):
        lis.extend(range(75)) 
        #print(lis)
    df["timepoint"] = lis
    sns.lineplot(x = "timepoint", y= what_to_plot, data = df, hue="File_path", legend = False)
    plt.xlabel("Time (min)")
    plt.ylabel("Protein Numbers on Chromosomes")

def align_time_series_chr_seg(df, what_to_plot):
    df_new = df.copy()
    df_new.name = df.name
    grouped = df_new.groupby("File_path")
    t_min = []
    for name, group in grouped:
        all = (group['Num_nuc']).count()
        count = (group['Num_nuc']-1).sum()
        ones = np.flip(np.arange(all-count)*-1)
        twos = np.arange(1,count+1)
        idx = np.concatenate([ones, twos])
        idx = idx * 2
        t_min.append(idx)
    fig, ax = plt.subplots()
#     ax.set_xlim(-3,30)
    df_new['time(min)'] = np.array(t_min).flatten()
    sns.lineplot(x = "time(min)", y= what_to_plot, data = df_new, hue="File_path", legend = False, ax=ax )
#     plt.xlabel("Time (min)")
#     plt.ylabel("Protein Numbers on Chromosomes")
    plt.axvline(x = 0, color = 'r', alpha =0.5, linestyle='--')
#     plt.text(x= 3.0, y =250000.0, s= 'chromosomes segregated', color = 'red', alpha = 0.5)
    return df_new

def align_time_series_AO(df, what_to_plot):
    df_new = df.copy()
    df_new.name = df.name
    grouped = df_new.groupby("File_path")
    t_min = []
    replicate_list = list()
    for name, group in grouped:
        # print(len(group))
        all = group['Ana_onset_flag'].count()
        count = group['Ana_onset_flag'].sum()
        count = count -1
        ones = np.flip(np.arange(all-count)*-1)
        twos = np.arange(1,count+1)
        idx = np.concatenate([ones, twos])
        idx = idx * 2
        t_min.append(idx)
        replicate_list += len(group) * [os.path.normpath(name).split(os.sep)[5]]
    df_new['time(min)'] = np.array(t_min).flatten()
    df_new['replicate'] = replicate_list
#     fig, ax = plt.subplots()
#     ax.set_xlim(-3,30)
#     sns.lineplot(x = "time(min)", y= what_to_plot, data = df_new, hue="File_path", legend = False, ax=ax )
#     plt.xlabel("Time (min)")
#     plt.ylabel("Protein Numbers on Chromosomes")
#     plt.axvline(x = 0, color = 'r', alpha =0.5, linestyle='--')
#     plt.text(x= 3.0, y =250000.0, s= 'Anaphase Onset', color = 'red', alpha = 0.5)
    return df_new

def plot_replicates(df, what_to_plot):
    fig, ax = plt.subplots(1,1)
    lis = [1,2,3]
    colors = ['black', 'blue', 'deeppink']
    color_pal = dict(zip(lis, colors))
    # print(color_pal)

    #make one df for every replicate
    color_iterator = 1
    d = {}
    for group_name, group in df.groupby('replicate'):
        d[str(group_name)] = group
    # print(d['replicate_20210601_STAG1#8-R2_MitoSys2'])
    for name, data in d.items():
        color = color_pal.get(color_iterator)
        print(color)
        color_iterator += 1
        sns.lineplot(x='time(min)', y=what_to_plot, data=data,color = color, units = 'File_path', estimator = None, ax = ax)

    plt.xlabel("Time (min)")
    plt.ylabel("Protein Numbers on Chromosomes")


def relative_N_nuc(df):
    """
    Normalize N_nuc to 120 min timepoint
    """
    df_new = df.copy()
    df_new.name = df.name
    grouped = df_new.groupby("File_path")
    N_nuc_relative = []
    length_QC = []
    for name, group in grouped:
        # print([group['time(min)'].loc[group['time(min)']==120]])
        try:
            relative_N = group['N_nuc'] / int(group['N_nuc'].loc[group['time(min)']==120].iloc[0])
            # print(relative_N)
            N_nuc_relative.append(relative_N)
            length_QC.extend([1 for i in range(len(group))])
        except:
            length_QC.extend([0 for i in range(len(group))])
    df_new['N_nuc_relative'] = pd.concat(N_nuc_relative)
    df_new['N_nuc_relative'] = round(df_new['N_nuc_relative']*100,0)
    df_new['length_QC'] = length_QC
    return df_new

def plot_mean(df, what_to_plot):
    fig, ax = plt.subplots()
    sns.lineplot(x = "time(min)", y= what_to_plot, data = df, ax=ax)
    ax.set_xlim(-5,25)
    ax.set_ylim(0,)
    plt.xlabel("Time (min)", fontsize = 13)
    plt.ylabel(what_to_plot, fontsize = 13)
    plt.axvline(x = 0, color = 'grey', alpha =0.7, linestyle='--')
#     plt.text(x= 5.0, y =455.0, s= 'Anaphase Onset', color = 'grey')
#     plt.legend(bbox_to_anchor=(1, 1), fontsize = 12)
    plt.show()