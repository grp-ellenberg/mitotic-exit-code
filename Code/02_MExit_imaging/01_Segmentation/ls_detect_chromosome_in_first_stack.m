function [chrRegion, centX, centY, centZ] = ls_detect_chromosome_in_first_stack(threeDLabel, stat, imgCentX, imgCentY, numPixels, dx, dy, Nz)
%This function detects chromsome in the first timepoint
chrRegion = zeros(dx,dy, Nz);
for i = 1: numel(stat)
    bDist = ((imgCentX-stat(i).Centroid(2))^2 + (imgCentY - stat(i).Centroid(1))^2)^3; %Special case you expect chr at centre;
    numPixels(i) = numPixels(i) * 1/bDist;
end
%Select the best - discard the one with artificial chromosome
chrRegion(:,:,:) = 0;
[~,idx] = max(numPixels);
chrRegion(threeDLabel.PixelIdxList{idx}) = 1;

centX = stat(idx).Centroid(2);
centY = stat(idx).Centroid(1);
centZ = stat(idx).Centroid(3);
end