function [V, R, xg, yg, zg] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ)
% Compute Eigen vectors from 3d points represented by x,y and z
% voxelSizeX, voxelSizeY, voxelSizeZ  are the size of voxel in x, y and z direction 
% Returns eigenvectors -V, eigenvalues - R, and centroids xg,yg and zg
xmic=x*voxelSizeX;
ymic=y*voxelSizeY;
zmic=z*voxelSizeZ;
xg=mean(xmic);
yg=mean(ymic);
zg=mean(zmic);
Txx=mean(xmic.^2)-xg^2;
Tyy=mean(ymic.^2)-yg^2;
Tzz=mean(zmic.^2)-zg^2;
Txy=mean(xmic.*ymic)-xg*yg;
Txz=mean(xmic.*zmic)-xg*zg;
Tyz=mean(ymic.*zmic)-yg*zg;
Tg=[Txx,Txy,Txz;Txy,Tyy,Tyz;Txz,Tyz,Tzz];

[V,R]=eig(Tg);
end