function [volume] = ls_remove_intermediate_slices(intVolume, zFactor)
%Removes interpolates slices from the volume to keep only original slices
[dx, dy, dz] = size(intVolume);

newDz = ceil(dz / zFactor);
volume = zeros(dx,dy,newDz);
for i = 1: newDz
    volume(:,:,i) = intVolume(:,:,i*zFactor-zFactor +1);
end
end