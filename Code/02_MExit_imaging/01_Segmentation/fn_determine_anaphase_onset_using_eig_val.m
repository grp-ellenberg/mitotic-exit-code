function [ana_onset_plus, eig_value1, eig_value2, eig_value3] = fn_determine_anaphase_onset_using_eig_val(curInDir, curOutDirMarker, inFileExt)
%This function detects anaphase onset from segmente chromsome mass using
%the smallest eigen value.
%Read metadata
tif_filename = dir([curInDir inFileExt]);
[reader, dim] = getBioformatsReader([curInDir tif_filename(1).name]);

%Initialize imaging parameters with the metadata
dx = dim.Ny; %Image height
dy = dim.Nx; % Image width
NzOrig = dim.Nz; %Number of Z slices in lsm file
Nc = dim.Nc; %Number of channels
tMax = length(tif_filename);

voxelSizeX = dim.voxelSize(1);
voxelSizeY =  dim.voxelSize(2);
voxelSizeZ_orig =  dim.voxelSize(3);
zFactor = round(voxelSizeZ_orig/voxelSizeX);
voxelSizeZ = voxelSizeZ_orig/zFactor; % voxelsize in Z gets closer to that in XY
matMarkerFn = dir([curOutDirMarker, '*.mat']);
%% Compute eigen values of detected chromosomes to detect anaphase onset
eig_values = zeros(length(matMarkerFn), 3); %Three eigenvalues for chrosomosomes
num_chr = zeros(length(matMarkerFn), 1); %number of detected chromsomes
ana_onset_plus = zeros(length(matMarkerFn), 1); %0 for pre Anaphase, 1 for the rest
for tpoint = 1:length(matMarkerFn)
    cur_mat = load([curOutDirMarker matMarkerFn(tpoint).name]);
    chr_mass = cur_mat.chromosomes; %load([curOutDirMarker matMarkerFn(tpoint).name], 'chromosomes');
    num_chr(tpoint) = cur_mat.numChr;
    
    [x,y,z] = ls_threed_coord(find(chr_mass), dx, dy);
    [~, cur_eig_values, ~, ~, ~] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ);
    eig_values(tpoint, 1) = cur_eig_values(1,1);
    eig_values(tpoint, 2) = cur_eig_values(2,2);
    eig_values(tpoint, 3) = cur_eig_values(3,3);
end

first_id_chr_seg = find(num_chr == 2,1,'first');
if ~isempty(first_id_chr_seg) %Need only if choromsome segregation has taken place
    if first_id_chr_seg == 2
        ana_onset_plus(:) = 1;
        
        %If we want to use a threshold of depth instead
        % if eig_values(1, 3)> depth_thresh_mic % depth_thresh_mic could be
        % determined from detected third (smallest eigen value with time
        % series where there are more time point before anaphase
        %    ana_onset_plus(:) = 1;
        %end
    
    elseif first_id_chr_seg == 3
        ana_onset_plus(1) = 0;
        ana_onset_plus(2:end) = 1;
        %If we want to use a threshold of depth instead
        % if eig_values(2, 3)> depth_thresh_mic
        %    ana_onset_plus(1) = 0;
        %    ana_onset_plus(2:end) = 1;
        %end
    else
        n= 6;
        n = min(n, first_id_chr_seg);
        n_small_eig_values = eig_values(first_id_chr_seg-n+1:first_id_chr_seg, 1);
        
        %minEigenTemp = n_small_eig_values;
        [minVal1, minIdx] = min(n_small_eig_values);
        n_small_eig_values(minIdx) = 10000; %make the first min detected eigenvalue huge such that it is not detected again as lowest. Therefore in the next round, the second lowest eigenvalue will be found
        [minVal2, minIdx] = min(n_small_eig_values);
        minVal = (minVal1+minVal2)/2;
        
        iter = 1;
        while eig_values(first_id_chr_seg-1,1) > minVal*2 && iter<=2 && first_id_chr_seg>=1
            first_id_chr_seg = first_id_chr_seg-1;
            iter = iter + 1;
        end
        ana_onset_plus(1:first_id_chr_seg-1) = 0;
        ana_onset_plus(first_id_chr_seg:end) = 1;
    end
end
eig_value1 = eig_values(:,1);
eig_value2 = eig_values(:,2);
eig_value3 = eig_values(:,3);

%% End of anaphase onset detection
end