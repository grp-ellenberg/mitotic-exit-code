function [procStat, paramsTable] = ls_detect_cell_membrane(curInDir, curOutDir, curOutDirMarker,...
    exParamsFn, cellChanIdx, chrChanIdx, poiChanIdx, nRemLowerSlices, chrSigInDxt, dispSegMass, inFileExt, cropFlag, cropDx, cropDy, calTable, paramsName, cell_op)
%% This function detects cell boundary and extracts different parameters
%  related chromosome/nucleus, cell and cytoplasm. It also extracts
%  absolute number of proteins, average concentration using the calibration
%  factors extracted using Politi et al. Nature Protocols, 2018

%% Parameters
% curInDir: Input data directory
% curOutDir: Output data directory
% curOutDirMarker: Directory containing chromosome markers saved by ls_detect_chromosomes
% exParamsFn: Name of the file storing the extracted parameters.
% cellChanIdx: Channel index of dextran
% chrChanIdx: Channel index of chromosome
% poiChanIdx: Channel index of poi
% nRemLowerSlices: Number of lower slices to be removed
% chrSigInDxt: Set 1 if chromosome signal is also prominent in dextral channel
% dispSegMass: 1 to visualize the segmented volumes/ 0 otherwise
% inFileExt: Extension of raw image data file
% cropFlag: Set 1 to crop, 0 otherwise
% cropDx: number of row in cropped image
% cropDy: number of column in cropped image
% calTable: Table containing the calibration factor and background intensity
% paramsName: name of the parameters to be stored
% cell_op: options/parameters value for cell segmentation

%%
% Author: M. Julius Hossain, EMBL, Heidelberg, Germany, julius.hossain@embl.de
% Created: 2014-11-13
% Last update: 2019-01-11

close all;
disp(['PROCESSING:' curInDir]);
if dispSegMass == 1
    dispDir = fullfile(curOutDir, 'DispSegMass', filesep);
    if ~exist(dispDir)
        mkdir(dispDir);
    end
end

tif_filename = dir([curInDir inFileExt]);
%tMax = length(tif_filename); %Number of lsm file in the folder
matMarkerFn = dir([curOutDirMarker, '*.mat']);

procStat = 0;

if length(tif_filename) ==0
    disp('Chromosome markers are missing or incomplete');
    return;
end

paramsTable = cell2table(cell(1,length(paramsName)));
paramsTable.Properties.VariableNames = paramsName;


cal_fact_nM = calTable.slope(1);
bg_int = calTable.baseline(1);

%Read metadata
[reader, dim] = getBioformatsReader([curInDir tif_filename(1).name]);

%Initialize imaging parameters with the metadata
dx = dim.Ny; %Image height
dy = dim.Nx; % Image width
NzOrig = dim.Nz; %Number of Z slices in lsm file
Nc = dim.Nc; %Number of channels
tMax = length(tif_filename);

voxelSizeX = dim.voxelSize(1);
voxelSizeY =  dim.voxelSize(2);
voxelSizeZ_orig =  dim.voxelSize(3);

tinit = 1; % Set higher than one if you do not want to process some timepoints before tinit
zinit = 1; % Set higher than one if you do not want to process some lower slices

%Update the parameters to create isotropic image
zFactor = round(voxelSizeZ_orig/voxelSizeX);
voxelSizeZ = voxelSizeZ_orig/zFactor; % voxelsize in Z gets closer to that in XY
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ;
voxelSize_orig = voxelSizeX * voxelSizeY * voxelSizeZ_orig;

Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1;
if Nc>=3
    chStackOrig = zeros(dx,dy,NzOrig-zinit+1);
    negStackOrig = zeros(dx,dy,NzOrig-zinit+1);
else
    disp('Stack contains less than three channels');
    return;
end

tpoint = tinit;
dataRead = 1;
%Read metadata
file_path = [curInDir tif_filename(tpoint).name];
%[reader, dim] = getBioformatsReader(file_path);
while tpoint <= tMax
    
    %Load detected chromosome/nuclear markers
    chrPack = load([curOutDirMarker matMarkerFn(tpoint).name]);
    [reader, dim] = getBioformatsReader([curInDir tif_filename(tpoint).name]);
    %Get image data
    chStackOrig  = getTPointBioFormats(reader, dim, 1, chrChanIdx);
    negStackOrig = getTPointBioFormats(reader, dim, 1, cellChanIdx);
    poiStackOrig = getTPointBioFormats(reader, dim, 1, poiChanIdx);
    
    %Crop images if needed
    if cropFlag == 1
        chStackOrig  = fn_crop_stack(chStackOrig, cropDx, cropDy, nRemLowerSlices);
        negStackOrig = fn_crop_stack(negStackOrig, cropDx, cropDy, nRemLowerSlices);
        poiStackOrig = fn_crop_stack(poiStackOrig, cropDx, cropDy, nRemLowerSlices);
        [dx, dy, NzOrig] = size(chStackOrig);
        Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation
    end
    
    %Generate intermediate slices to make isotropic volumes
    chStack = ls_gen_intermediate_slices(chStackOrig, zFactor);
    negStack = ls_gen_intermediate_slices(negStackOrig, zFactor);
    chStackBack = chStack;
    negStackBack = negStack;
    if tpoint == tinit
        tempVol = zeros(dx,dy,Nz);   %Store current segmented volume  as a referece for next/prev time point
        cRegion = zeros(dx,dy,Nz);   %Store the detected cytoplasm region
        chRegion = zeros(dx,dy,Nz);  %Store the detected chromosome region
    end
    
    %Apply Gaussian filter
    chStack = imgaussian(chStack,cell_op.sigmaBlur,cell_op.hSizeBlur);
    negStack = imgaussian(negStack, cell_op.sigmaBlur, cell_op.hSizeBlur);
    
    chStackSeg = imgaussian(chStackBack, cell_op.sigmaChSeg, cell_op.hSizeChSeg);
    
    disp(['Detecting cell volume: tpoint' num2str(tpoint, '%02d')]);
    %Get the thresholds used for chromosome detection
    chThresh2D = chrPack.chrThresh2D;
    chThresh3D = chrPack.chrThresh3D;
    if chrSigInDxt == 1
        chRegion(:,:,:) = 0;
        %Combine 2D and 3D thresholds for binarizing different slices
        for i = 1:Nz
            chRegion(:,:,i) = double(chStackSeg(:,:,i) >= (chThresh3D * (1-cell_op.bFactorCh2D) + chThresh2D(i) * cell_op.bFactorCh2D));
        end
        
        chStackNorm = chStack;
        chStackNorm(chRegion(:,:,:) == 1) = 0;
        sumBgAvgInt = 0;
        for i = 1: Nz
            totalBgInt = sum(sum(chStackNorm(:,:,i)));
            totalBgPix = sum(sum(chRegion(:,:,i)==0));
            avgBgInt = totalBgInt/totalBgPix;
            tempFrame = chStack(:,:,i);
            tempFrame(chStackSeg(:,:,i) < avgBgInt) = avgBgInt;
            chStackNorm(:,:,i) = tempFrame;
            sumBgAvgInt = sumBgAvgInt + avgBgInt;
        end
        gAvgBgInt = sumBgAvgInt/Nz;
        ratioImage = (gAvgBgInt * negStack)./chStackNorm;
        
        ratioImage = imgaussian(ratioImage, cell_op.sigmaRatio, cell_op.hSizeRatio);
        clear chStackNorm;
        clear tempFrame;
    else
        ratioImage = negStack;
    end
    
    %% Correcting cytomplamic intensity on top of the nucleus
    z_sum = sum(sum(chrPack.chromosomes));
    top_chr_idx = find(z_sum>0, 1, "last");
    crop_start_idx = min(Nz, top_chr_idx + round(cell_op.chr_mem_gap_mic/voxelSizeZ));
    tot_int = sum(sum(sum(ratioImage(:,:,crop_start_idx:end))));
    %max_int = max(max(max(ratioImage(:,:,crop_start_idx:end))));
    tot_pix = dx *dy * (Nz - crop_start_idx+1);
    avg_int = tot_int/tot_pix;
    if crop_start_idx < Nz
        while crop_start_idx<=Nz
            cur_image = ratioImage(:,:,crop_start_idx);
            cur_image(cur_image<=avg_int) = avg_int;
            ratioImage(:,:,crop_start_idx) = cur_image;
            crop_start_idx = crop_start_idx + 1;
        end
    end

    %%
    %ratioImage(ratioImage(:,:,crop_start_idx:end)<avg_int) = avg_int;
    %Generate inverse image from negative staining channnel
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    negNumVoxels = dx*dy*Nz*cell_op.negVoxelsRatioInv;
    sumVoxels = 0;
    for i = length(histRatio):-1: 1
        sumVoxels = sumVoxels + histRatio(i);
        if sumVoxels >= negNumVoxels
            break;
        end
    end
    
    ratioImage = i - ratioImage;
    ratioImage(ratioImage(:,:,:) <0) = 0;
    
    %Get thresholds and binarize ratio/inverted image
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    cThresh3D = cThresh3D *cell_op.thrDownFact;
    cRegion(:,:,:) = 0;
    for i = 1:Nz
        cThresh2D = ls_otsu_2d_image(ratioImage(:,:,i), 0);
        cThresh2D = cThresh2D *cell_op.thrDownFact;
        cRegion(:,:,i) = double(ratioImage(:,:,i) >= (cThresh3D * (1-cell_op.bFactorRatio) + cThresh2D * cell_op.bFactorRatio));
    end
    
    bgMaskInt = cRegion;
    threeDLabel = bwconncomp(cRegion,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    for i = 1 : numel(numPixels)
        if numPixels(i) * voxelSize < cell_op.minVol
            cRegion(threeDLabel.PixelIdxList{i}) = 0;
        end
    end
    
    %% Filling of thresholded image
    for zplane = 1: Nz
        cRegion(:,:,zplane)=imdilate(cRegion(:,:,zplane),strel('disk',cell_op.rCRegion,0));
        cRegion(:,:,zplane)=imfill(cRegion(:,:,zplane),'holes');
        bgMaskInt(:,:,zplane)=imdilate(bgMaskInt(:,:,zplane),strel('disk',cell_op.rCRegion,0));
        bgMaskInt(:,:,zplane)=imfill(bgMaskInt(:,:,zplane),'holes');
        for i=1:cell_op.rCRegion
            cRegion(:,:,zplane)=imerode(cRegion(:,:,zplane),strel('diamond',1));
        end
    end
    
    bgMaskInt = 1-bgMaskInt;
    bgStack = negStackBack;
    bgStack(bgMaskInt(:,:,:) == 0) = 0;
    
    %% Combining distance transform and watershed to identify cell region of interest
    distImage = bwdistsc(~cRegion,[1 1 voxelSizeZ/voxelSizeX]);
    
    cRegionEroded = imerode(cRegion, cell_op.se);
    
    chrPack.chrMarker(cRegionEroded(:,:,:) == 0) = 0;
    chrPack.chrMarker(:,1,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(1,:,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(:,dy,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(dx,:,round(Nz/20):round(Nz/2)) = 1;
    
    clear cRegionEroded;
    
    distImage = -distImage; % invert distance image
    %displaySubplots(distImage, 'DT of inverse', disRow, disCol, Nz, zFactor, 2);
    distImage = imimposemin(distImage,chrPack.chrMarker, 18); %suppress local minima other than markers
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
    distImage(~cRegion) = -Inf; % Set outside of cell region with infinity
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
    wsLabel = watershed_old(distImage); %Apply watershed algorithm
    
    wsLabel(~cRegion) = 0;
    %fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    
    threeDLabel = bwconncomp(wsLabel,18);
    wsLabel(:,:,:) = 0;
    for i = 1: threeDLabel.NumObjects
        wsLabel(threeDLabel.PixelIdxList{i}) = i;
    end
    
    if chrPack.numChr == 1
        lbl1 = fn_get_ws_label_on_chromosome(wsLabel,chrPack.chromosomes, 1);
        wsLabel(wsLabel(:,:,:) ~= lbl1) = 0;
    else
        tempVol(:,:,:) = 0;
        nuc1 = double (chrPack.chromosomes(:,:,:) == 1);
        nuc1 = nuc1.*wsLabel(:,:,:);
        [vals, a, b]=unique(nuc1);
        a_counts = accumarray(b,1);
        value_counts = [vals, a_counts];
        [~, idx] = max(value_counts(2:end,2));
        lbl1 = value_counts(idx+1,1);        
        tempVol(wsLabel(:,:,:) == lbl1) = 1;
        
        nuc2 = double (chrPack.chromosomes(:,:,:) == 2);
        nuc2 = nuc2.*wsLabel(:,:,:);
        [vals, a, b]=unique(nuc2);
        a_counts = accumarray(b,1);
        value_counts = [vals, a_counts];
        [~, idx] = max(value_counts(2:end,2));
        lbl2 = value_counts(idx+1,1);
        tempVol(wsLabel(:,:,:) == lbl2) = 1;
        
        wsLabel = tempVol;
    end
    wsLabel(wsLabel(:,:,:)>0) = 1;
    
    %fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    wsLabel = imclose(wsLabel, cell_op.se);
    %To deal with the filtering
    wsLabel = imclose(wsLabel,cell_op.se);
    wsLabel = imdilate(wsLabel,cell_op.se);
    
    %Store  initial detected cell region as 'cRegion'
    cRegion = wsLabel;
    %displaySubplots(cRegion, 'Selected blob using Watershed + DT raw', disRow, disCol, Nz, zFactor, 2);
    
    %% Removing small object from binary images
    cRegionDis = cRegion;
    cRegion(:,:,:) = 0;
    for zplane=1:Nz
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
        if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=cell_op.minObjArea
            continue;
        end
        
        cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
        cRegion(:,:,zplane) = curBlob;
        
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
        if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=cell_op.minObjArea
            continue;
        end
        cRegion(:,:,zplane) = or(cRegion(:,:,zplane), curBlob);
    end
    threeDLabel = bwconncomp(cRegion,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    [~, idx] = max(numPixels);
    cRegion(:,:,:) = 0;
    cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    numPixels(idx) = 0;
    [biggest, idx] = max(numPixels);
    if biggest *voxelSizeX * voxelSizeY * voxelSizeZ >= cell_op. minCellVolume
        cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    end
    % Perform morpholical operation to smooth detected object
    refVol = sum(sum(sum(cRegion))) * voxelSizeX * voxelSizeY * voxelSizeZ;
    cRegion = ls_smooth_and_equalize(cRegion, refVol, voxelSizeX*voxelSizeY*voxelSizeZ);
    
    for i=1:Nz
        cRegion(:,:,i) = imfill(cRegion(:,:,i), 'holes');
    end
    
    for i = round(Nz*0.7):Nz-1
        cRegion(:,:,i+1) = double(and(cRegion(:,:,i+1), cRegion(:,:,i)));
    end
    chrPack.chromosomes(cRegion(:,:,:) == 0) = 0;
    
    %%
    %%Detect chr_dist
    if chrPack.numChr == 1
        chr_dist_mic_2d = 0;
        chr_dist_mic_3d = 0;
    else
        chr_region1 = double(chrPack.chromosomes(:,:,:) ==1);
        linIndex = find(chr_region1 >0);
        [xCoords, yCoords, zCoords] = ind2sub(size(chr_region1),linIndex);
        curCentX1 = sum(xCoords)/length(xCoords);
        curCentY1 = sum(yCoords)/length(yCoords);
        curCentZ1 = sum(zCoords)/length(zCoords);

        chr_region2 = double(chrPack.chromosomes(:,:,:) ==2);
        linIndex = find(chr_region2 >0);
        [xCoords, yCoords, zCoords] = ind2sub(size(chr_region2),linIndex);
        curCentX2 = sum(xCoords)/length(xCoords);
        curCentY2 = sum(yCoords)/length(yCoords);
        curCentZ2 = sum(zCoords)/length(zCoords);
        
        chr_dist_mic_2d = norm([curCentX1 curCentY1].*[voxelSizeX voxelSizeY] - [curCentX2 curCentY2].*[voxelSizeX voxelSizeY]);
        chr_dist_mic_3d = norm([curCentX1 curCentY1 curCentZ1].*[voxelSizeX voxelSizeY voxelSizeZ] - [curCentX2 curCentY2 curCentZ2].*[voxelSizeX voxelSizeY voxelSizeZ]);
    end
    %%

    %% Extract parameters and save the results in mat file
    nucVolMic  = sum(sum(sum(chrPack.chromosomes(:,:,:)>0))) * voxelSize;
    cellVolMic = sum(sum(sum(cRegion(:,:,:)>0))) * voxelSize;
    cytVolMic  = sum(sum(sum(chrPack.chromosomes(:,:,:)==0 & cRegion(:,:,:)>0))) * voxelSize;
    numChr = chrPack.numChr;
    %Remove intermediate slices to saver the results in original z
    %resolution
    cellMass   = ls_remove_intermediate_slices(cRegion, zFactor);
    chrMass    = ls_remove_intermediate_slices(chrPack.chromosomes, zFactor);
    poi = poiStackOrig;
    
    cellVolPix = sum(sum(sum(cellMass(:,:,:)>0)));
    nucVolPix  = sum(sum(sum(chrMass(:,:,:)>0)));
    cytVolPix  = sum(sum(sum(chrMass(:,:,:)==0 & cellMass(:,:,:)>0)));
    
    cellTotInt = sum(sum(sum(poi(cellMass>0))));
    nucTotInt  = sum(sum(sum(poi(chrMass>0))));
    cytTotInt = sum(sum(sum(poi(chrMass(:,:,:)==0 & cellMass(:,:,:)>0))));
    
    voxelSizeMic = voxelSize_orig;
    
    % Calculate total number of proteins in each compartment
    N_cell = (cellTotInt - cellVolPix*bg_int) *cal_fact_nM*voxelSizeMic*cell_op.NA;
    N_nuc  = (nucTotInt  - nucVolPix *bg_int) *cal_fact_nM*voxelSizeMic*cell_op.NA;
    N_cyt  = (cytTotInt  - cytVolPix *bg_int) *cal_fact_nM*voxelSizeMic*cell_op.NA;
    
    % Generate avarage concentration in each compartment
    con_cell_nM = (cellTotInt/cellVolPix -bg_int)*cal_fact_nM;
    con_nuc_nM  = (nucTotInt/nucVolPix -bg_int)  *cal_fact_nM;
    con_cyt_nM  = (cytTotInt/cytVolPix -bg_int)  *cal_fact_nM;
    
    % Save the results in mat file
    savefile = [curOutDir matMarkerFn(tpoint).name];
    save(savefile,'file_path', 'N_cell', 'N_nuc', 'N_cyt', 'con_cell_nM', 'con_nuc_nM', 'con_cyt_nM', 'cellTotInt', 'nucTotInt', 'cytTotInt','cellMass', 'chrMass', 'poi', 'nucVolMic', 'cellVolMic', 'cytVolMic', 'voxelSizeMic', 'cellVolPix', 'nucVolPix', 'cytVolPix', 'numChr', 'chr_dist_mic_2d', 'chr_dist_mic_3d');
    
    %Create a table to store the parameters
    newTable = cell2table({file_path, N_cell, N_nuc, N_cyt, con_cell_nM, con_nuc_nM, con_cyt_nM, cellTotInt, nucTotInt, cytTotInt, cellVolMic, nucVolMic, cytVolMic, cellVolPix, nucVolPix, cytVolPix, cal_fact_nM, bg_int, numChr, chr_dist_mic_2d, chr_dist_mic_3d});
    newTable.Properties.VariableNames = paramsName;
    paramsTable = cat(1,paramsTable,newTable);
    
    %Render the segmented volumes
    if dispSegMass == 1
        [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
        hVol = figure('Name', strcat('Frame: ', num2str(tpoint, '%0.3f')), 'NumberTitle','off', 'Visible', 'off');
        hVol = fn_plot_3d_chromosome_cell_surfaces(hVol, X, Y, Z, chrPack.chromosomes, cRegion, cell_op.thresh, cell_op.color, cell_op.alpha);
        
        savefile = [dispDir tif_filename(tpoint).name(1:end-4) '_cell.jpg'];
        saveas(hVol, savefile, 'jpg');
        delete(hVol);
    end
    
    clear cellVolume;
    clear nucVolume;
    clear cellRegion;
    clear chrRegion;
    clear numChr;
    clear wsLabel;
    clear distImage;
    clear chrMarker;
    clear hist;
    close all;
    clear chrPack;
    tpoint = tpoint + 1;
end

%Save the parameters in a tab delimited text file
paramsFn = fullfile(curOutDir,  exParamsFn);
paramsTable(1,:) = [];
writetable(paramsTable,paramsFn,'Delimiter','\t');
procStat = 1;
end