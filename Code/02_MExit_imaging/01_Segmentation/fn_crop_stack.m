function [outStack] = fn_crop_stack(inStack,cropDx, cropDy, nRemoveLowerSlices)
% This function crops inStack from image centre in xy and removes nRemoveLowerSlices 
% number slice from the lower part of the stack  

[inDx, inDy, inDz] = size(inStack);

if (cropDx >= inDx || cropDy >= inDy) && nRemoveLowerSlices ==0
    outStack = inStack;
    return;
end

outDz = inDz - nRemoveLowerSlices;
outStack = zeros(cropDx, cropDy, outDz);
if cropDx < inDx && cropDy < inDy

    centX = round(inDx/2);
    centY = round(inDy/2);

    halfCropX = round(cropDx/2);
    halfCropY = round(cropDy/2);

    for zplane = 1: outDz
        outStack(:,:,zplane) = inStack(centX-halfCropX:centX+halfCropX-1, centY-halfCropY:centY+halfCropY-1, zplane + nRemoveLowerSlices);    
    end
else
    for zplane = 1: outDz
        outStack(:,:,zplane) = inStack(:, :, zplane + nRemoveLowerSlices);    
    end
end
end

