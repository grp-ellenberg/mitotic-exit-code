# 06 Code for Analysis of processed LoopTrace data

LoopTrace raw imaging data was converted from .nd2 to OME-Zarr file format and processed according to the [LoopTrace documentation](https://git.embl.de/grp-ellenberg/looptrace).

### Usage Instructions

* Install [LoopTrace_analysis](https://git.embl.de/grp-ellenberg/looptrace/-/blob/master/environment_analysis.yml?ref_type=heads) environment and packages
* Import processed LoopTrace data from [BioImage Archive Repository](https://www.ebi.ac.uk/biostudies/bioimages/studies/S-BIAD1454)
* run notebook cells to recapitulate analysis and plots shown in the paper.

