# 03 Code for Analysis of mitotic exit protein depletions

## Segmentation
The Python-based pipeline segments nuclei in .nd2 image files semi-manually and extracts fluorescence intensity parameters.

Includes packages that can be installed as [environment](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/03_MExit_protein_depletions/Depletion_IF_environment.yml?ref_type=heads).

### Fluorescence intensity data extracted from segmented nuclei (.csv files) for all main manuscript figures are provided [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/03_MExit_protein_depletions?ref_type=heads)

## Analysis

Main analysis script which performs:
* background intensity correction (experimentally determined camera background)
* plotting of DAPI, AID-GFP (ch1, depletion control) and IF channel (ch2 or ch3(=RAD21))