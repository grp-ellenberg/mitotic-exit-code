# Description of the R-based data processing script, especially its getdata() getfraptrace() and getaverage() functions 

This data processing script is a reduced version of [Politi et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018/-/tree/master/FRAP?ref_type=heads).
Downstream analysis (visualization, fitting, analysis, plotting) was implemented in a custom Python script.

# Usage instruction
* load packages and function definitions
* define database file folder path and database file name
* run script until output saving
* make sure to have a "results" folder present in the same folder as the input
* save csv files containing the normalized informaton for bleached and unbleached dataframes.

## The getdata(function):
    input: getdata(filepaths, zslice = 5, maskthr = 0.1)
1) reading in the lineROI data (all z-slices) separately for chr, poi and mask and storing them as dataframes called chr, poi and mask
2) remove mask where mask < threshold
3) get the maximum length of a lineROI
4) updates the ch and poi values based on the mask values: poi_val_new = poi_val/mask_val  
e.g. POI_val = 251, mask_val = 85 -> poi_val_new = 251:85 = 2.95  
This serves the purpose to get the mean over the mask and not the mean over the square.
5)  the information of a specific z-slice is extracted and set as value for chr, poi and mask (= zslice selection)  
6)  The normalized lineROI values for a selected z-slice are returned as a list of 3 dataframes: chr, poi and mask.


## the getaverage() function:
**Defined before the getfraptrace() function as it will be called within that to average the mean profiles (mean over z-slices) into bl & unbl values.**
        
    Input: getaverage(profile, offset = 7)

01) get max length of lineROI and save it
02) transpose the mprofile poi and ch dataframes such that timepoints are now not rows, but columns and pixels of the lineROI are rows not columns,  
-> save transposed dfs as _poi_val_ and _ch_val_  
the first 3 columns (with lineROI index, z-slice index etc were cropped off here)  
1)  assign _poi_val_ to variable _poi_w_  
_poi_val_ is then used for the chromatin-unweighted average across the bleached/unbleached regions  
_poi_w_  for the weighted averages 
1)  ask for dim() of poi_val and extract xwidth and maxtime from it
2)  start a for loop iterating through every timepoint from t=1 to t=maxtime:
    - assign left half of lineROI indices (offset considered) to vec1
    - assign right half of lineROI indices to vec2
    - multiply all _poi_w_ pixel intensity values inside vec1 with the respective chromatin values that are normalized to the sum of all chromatin intensity values -> if lineROI is longer, it does not mean it will have a brighter intensity. assign normalized vec1 values to poi_w[vec1, timepoint]
    - do the same for vec2
3)  sum up all pixel values from the unbleached region for each timepoint respectively (sum of the entire vec1 part of the colum), assign to poi_unbl
4)  do the same for vec2, assign to poi_bl
5)  put the data from poi_unbl and poi_bl into two new dataframes.   
columns: 1) time 2) sum of unbleached or sum of bleached region 3) unnormalized mean of unbleached/bleached region = unweighted  
**general time can also be adapted if time intervals were != 20 s.**
1)  return poi_unbl and poi_bl as list of two elements (prbl dataframe)  
-> poi_unbl and poi_bl are now saved in getfraptrace() funciton in variable _avg_poi_ as avg_poi$poi_unbl and avg_poi$poi_bl

## The getfraptrace() function:

extract avgpoi data for one cell:
Compute differene in fluorescence intensities in bleached and unbleached region over n z-slices and normalize == _avg_poi_  
@details The POI intensity mean is weighted by the DNA intensities. This ensures that POI on DNA is accounted for rather than POI in cytoplasm.   
The function calls *getdata()* & *getaverage()* for the computation.  

        Input: getfraptrace(filepaths, cellID = '', POI = '', offset = 7, zslices = c(4,5,6))) 

1)  Collect profiles to average: Get data (by using getdata()) for all selected zslices and store them in profile list:  
=3 dataframes per z-slice: 1 for chr, 1 for POI, 1 for mask and saves them in a dataframe.  
profile list contains as many elements as z-slices that were selected

1)  Compute mean of profile for each time point:
    - assign the information of one of the z-slices to mprofile, I think this is required to set the dimensions of the dataframe
    - for loop 1 starts: for every timepoint:   
        (1) a list tmp_poi is created  
        (2) a list ch_poi is created  
            -> for loop 2 starts:  
            iterate through every of the selected z-slices to:  
            -> combine the current version of the tmp_poi list with the profile data from each z-slice i for the timepoint t.    
            = the lineROI data of every zslices of the given timepoint is collected in tmp_poi (dataframe with x rows (x = number of selected zslices, each row is the respective z-slice info)   
            -> same for tmp_ch   
            note: here, poi data is added in original script, should be ch data that is added:  
            !!! line 107: tmp_ch <- rbind(tmp_ch, profile[[i]]$poi[t,]) profile data of poi, not ch is used  
    - the data stored in tmp_poi and tmp_ch is averaged (average of 3 zslices is calculated for timepoint of interest)
    - stores the averaged timepoint information in mprofile
    - mprofile dataframe now contains the mean profile data across the chosen z-slices
    
2)  call **getaverage()** to average the mprofile data in two classes bleached and unbleached  
@description Use profiles generated from getdata() or the profiles averaged across z-slices from getfraptrace to compute average poi intensities on the bleached (right of center DNA) and unbleached (left of center DNA) regions.

1)  assign cellID to avg_poi data - a column full of rows with the cellID

2)  calculate FRAP curves:  
    - calculate poi_norm: (FI(t) - FI(t = afterbleach))/(FI(0) - FI(t=afterbleach))
        note: the weighted poi intensities are used
    - same is done for unbleached data
    - assigns the POI name to the new 'POI 'column but only for the poi_unbl, poi_bl follows later, bit confusing this way but ok
    - Calculates the Gerlich et al. 2006 style of representing data. 
        The advantage is that we don't need to compute the bleaching parameters
    - subtracts poi_bl data from poi_unbl data and assigns it to avg_poi$poi_bl$diff
    - normalizes diff  by division through the subtracted value at time 0 (=afterbleach) -> norm_diff
    - assigns the POI name to the new 'POI 'column now also for poi_bl
3)  returns avg_poi which contains poi_unbl dataframe and poi_bl dataframe 

