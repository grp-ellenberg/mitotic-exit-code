### This script defines general functions for FRAP analysis
### Author: Andreas Brunner, EMBL, Heidelberg
### Last update: 2023-02-27

# Import dependencies

import pandas as pd
import seaborn as sns
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq
from scipy.optimize import least_squares
from scipy.optimize import curve_fit

#for plot_individuals_by_chr_volume function
import matplotlib.colors as mcolors
import matplotlib.cm as cm
from matplotlib.cm import ScalarMappable

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

##################################################### Overview plots
##### function to plot average time-series
def plot_avgs(data, plot, save = False, path = None, format = 'svg', crop = False, color = 'grey'):
# crop data if wanted
    if crop == True:
        data = data.loc[data['time'] >=0]
# plotting:
    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(7)
    sns.lineplot(x = "time", y= str(plot), data = data, ax=ax, color = color, errorbar = 'sd')
#labels:
    plt.ylim(0,1.1)
    plt.xlabel("Time (s)", fontsize = 15)
    if plot == 'diff_norm':
        plt.ylabel(r'$\frac{F[ub](t)-F[b](t)}{F[ub](0)-F[b](0)}$', fontsize = 20)
    else:
        plt.ylabel(str(plot), fontsize = 20)
#saving:
    if save == True:
        plt.savefig(path, dpi=500, format= format)


##### function to plot individual time-series by cellID
def plot_individuals(data, plot, log_transform = False, save = False, path = None, format = 'svg', crop = False):
# crop data if wanted
    if crop == True:
        data = data.loc[data['time'] >=0]
# plotting:
    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(7)
    sns.lineplot(x = "time", y= str(plot), data = data, ax=ax, hue = 'cellID')
    if log_transform == True:
        ax.set_yscale('log')
    else:
        pass
    ax.get_legend().remove()
#labels:
    plt.ylim(0,1.1)
    plt.xlabel("Time (s)", fontsize = 15)
    if plot == 'diff_norm':
        plt.ylabel(r'$\frac{F[ub](t)-F[b](t)}{F[ub](0)-F[b](0)}$', fontsize = 20)
    else:
        plt.ylabel(str(plot), fontsize = 20)
#saving:
    if save == True:
        plt.savefig(path, dpi=500, format= format)

##### compare two datasets
def plot_compare(data1, data2, plot = 'diff_norm', color1 = 'grey', color2 = 'blue', crop = False, ylim = 0):
    if crop == True:
        data1 = data1.loc[data1['time'] >=0]
        data2 = data2.loc[data2['time'] >=0]

    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(6)
    sns.lineplot(x = "time", y= str(plot), data = data1, ax=ax, color = color1)
    sns.lineplot(x = "time", y= str(plot), data = data2, ax=ax, color = color2)

    plt.xlabel("Time (s)", fontsize = 15)
    plt.ylim(ylim,)
    if plot == 'diff_norm':
        plt.ylabel(r'$\frac{F[ub](t)-F[b](t)}{F[ub](0)-F[b](0)}$', fontsize = 20)
    else:
        plt.ylabel(str(plot), fontsize = 20)


def plot_compare_avg_3(data1, data2, data3, plot = 'diff_norm', color1 = 'grey', color2 = 'blue', color3 = 'red', crop = False):
    if crop == True:
        data1 = data1.loc[data1['time'] >=0]
        data2 = data2.loc[data2['time'] >=0]
        data3 = data3.loc[data3['time'] >=0]

    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(6)

    sns.lineplot(x = "time", y= str(plot), data = data1, ax=ax, color = color1, alpha = 0.5, errorbar = 'sd')
    sns.lineplot(x = "time", y= str(plot), data = data2, ax=ax, color = color2, errorbar = 'sd')
    sns.lineplot(x = "time", y= str(plot), data = data3, ax=ax, color = color3, alpha = 0.6, errorbar = 'sd')
    plt.ylim(0,1.05)
    plt.xlabel("Time (s)", fontsize = 15)

    if plot == 'diff_norm':
        plt.ylabel(r'$\frac{F[ub](t)-F[b](t)}{F[ub](0)-F[b](0)}$', fontsize = 20)
    else:
        plt.ylabel(str(plot), fontsize = 20)

########## plot time-series with chromosomal volume as hue
def plot_individual_by_chr_volume(df):
    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(6)
    #prepare dataset:
    chr_vol_mic3_list = [int(x)  for x in df['chr_volume_mic3']]
    nValues = np.array(chr_vol_mic3_list)
    data = df.loc[df['time'] >=0]
    # setup the normalization and the colormap
    normalize = mcolors.Normalize(vmin=nValues.min(), vmax=nValues.max())
#     cmap = mcolors.LinearSegmentedColormap.from_list("", ["grey","red", "blue","orange"])
    cmap = sns.color_palette("flare", as_cmap=True)
    # plot colorbar for reference
    cmappable = ScalarMappable(norm=normalize, cmap=cmap)
    cbar = plt.colorbar(cmappable)
    cbar.set_label('chr vol mic3', rotation=270)
    
    sns.lineplot(x = "time", y= "diff_norm", data = data, hue = 'chr_volume_mic3', palette = cmap, alpha = 0.7, legend = None)
    plt.ylim(-0.1,1.1)
    plt.xlabel("Time (s)", fontsize = 15)
    plt.ylabel(r'$\frac{F[ub](t)-F[b](t)}{F[ub](0)-F[b](0)}$', fontsize = 20)
    plt.show()

################################################## Calculation of bound fractions

def bound_fract_corr_cell(unbl, bl):
    '''
    Function description
    takes cell measurement of unbl and bl dataframe for a single cell to calculate bound fraction of POI corrected for remaining soluble pool (as examined in bl)
    function for computation on individual cells, not dataframes, 
    for integration in test_model_fits
    '''
    unbl_prebleach = unbl['poi_uw'].iloc[0]
    unbl_postbleach = unbl['poi_uw'].iloc[1]
    bl_postbleach = bl['poi_uw'].iloc[1]
    # calculate bound fraction
    bound_fract = (unbl_postbleach - bl_postbleach) / unbl_prebleach
    # printing of individual values for checking
    # print("unbl_prebleach " + str(unbl_prebleach))
    # print("unbl_postbleach " + str(unbl_postbleach)) 
    # print("bl_postbleach " + str(bl_postbleach)) 
    # print(bound_fract) 
    return bound_fract


##############################################################################################################################################
###                                                            FUNCTIONS FOR FITTING                                                       ###
##############################################################################################################################################

############################### single exponential, no IF
def mod_noIF(tau, t):
    return np.exp(-t/tau)

#define residual function
def residual_mod_noIF(tau, t, diff_norm):
    return diff_norm - mod_noIF(tau, t) #defines a function that calculates the residual of the substraction of experimental data with a fit


############################### single exponential, IF
def mod_IF(p, t):
    return p[0]+(1-p[0])*np.exp(-t/p[1])

#define residual function
def residual_mod_IF(p, t, diff_norm):
    return diff_norm - mod_IF(p, t) #defines a function that calculates the residual of the substraction of experimental data with a fit


############################### double exponential, no IF
def mod_2exp(p, t): 
    return p[0]*np.exp(-t/p[1]) + (1-p[0])*np.exp(-t/p[2])

#define residual function
def residual_mod_2exp(p, t, diff_norm):
    return diff_norm - mod_2exp(p, t) #defines a function that calculates the residual of the substraction of experimental data with a fit


############################### double exponential, IF
#double exponential model with IF
# def mod_2exp_IF(p, t):
# def mod_2exp_IF(t,p): #rewritten to fit curve_fit requirements
#     return p[0] + p[1]*np.exp(-t/p[2]) + (1-(p[0]+p[1]))*np.exp(-t/p[3])
def mod_2exp_IF(t, p0, p1, p2, p3): 
    return p0 + p1*np.exp(-t/p2) + (1-(p0+p1))*np.exp(-t/p3)

# #define residual function. 
# def residual_mod_2exp_IF(p, t, diff_norm):
#     return diff_norm - mod_2exp_IF(p, t)  #defines a function that calculates the residual of the substraction of experimental data with a fit
def residual_mod_2exp_IF(p, t, diff_norm): #input had to be changed from (p,t) to (t, *p) because of requirements of curve_fit function
    return diff_norm - mod_2exp_IF(t, *p)  #defines a function that calculates the residual of the substraction of experimental data with a fit


#define function to subset dataframe to time>0 and to current time-series:
def subset_df(df, time_series, start_timepoint):
    #initiate new dataframe to fill with cropped time-series
    df_new = pd.DataFrame()
    df_new["time"] = df["time"].loc[(df["time"]>=start_timepoint*20) & (df["cellID"]==time_series)]
    df_new["time"] = df["time"] - (20 * start_timepoint) #adjust time according to cropping to start plotting at t=0
    df_new["diff_norm"] = df["diff_norm"].loc[(df["time"]>=start_timepoint*20) & (df["cellID"]==time_series)]
    df_new["chr_volume_mic3"] = df["chr_volume_mic3"].loc[(df["time"]>=start_timepoint*20) & (df["cellID"]==time_series)]
    df_reset = df_new.reset_index(drop=True)
    # normalization to 1 in case start timepoint is not set to 0
    df_reset["diff_norm"] = df_reset["diff_norm"]*(1/df_reset["diff_norm"][0])
    # print(df_reset["time"])
    # print(df_reset["diff_norm"])
    # print(df_reset.loc[df_reset['time']==0])
    return df_reset


#define function to calculate R-squared to assess goodness of a fit
def r_squared(df, popt, model):
    # #calculate covariance to assess goodness of fit:
    if model == 'mod_2exp_IF':
        y_fit = globals()[model](df["time"], *popt)
    else:
        y_fit = globals()[model](popt, df["time"]) #calculates y-data based on determined optimized parameters and model
    # the globals()[model] allows to call the respective exponential model function based on the string input 
    # which model to take. uses the time_real of the time-series as x values to determine y values exactly at those times.
    #calculate r-squared based on corrlation matrix of the actual y-intensity data and the fit data
    correlation_matrix = np.corrcoef(df["diff_norm"], y_fit)
    correlation_xy = correlation_matrix[0,1]
    r_squared = correlation_xy**2
    return r_squared


#define function to carry out least squares fitting:
def least_squares(p0, df, model):
    if model == "mod_noIF": #to fit data to first model
        popt, pcov = leastsq(residual_mod_noIF, p0, args=(df["time"], df["diff_norm"])) #optimizes the parameters by lowering the residuals, pcov is covariance estimate
        xn = np.linspace(0, 600, 600) #set x values to plot 
        yn = mod_noIF(popt, xn) #calculate y values to plot
        r2 = r_squared(df, popt, model)
    elif model == "mod_IF":
        popt, pcov = leastsq(residual_mod_IF, p0, args=(df["time"], df["diff_norm"])) #optimizes the parameters by lowering the residuals, pcov is covariance estimate
        xn = np.linspace(0, 600, 600) 
        yn = mod_IF(popt, xn)
        r2 = r_squared(df, popt, model)
        # update immobile fraction:
        # popt[0] = popt[0] - float(df['offset'][0])
    elif model == "mod_2exp":
        popt, pcov = leastsq(residual_mod_2exp, p0, args=(df["time"], df["diff_norm"])) #optimizes the parameters by lowering the residuals, pcov is covariance estimate
        xn = np.linspace(0, 600, 600) 
        yn = mod_2exp(popt, xn)
        r2 = r_squared(df, popt, model)
    elif model == "mod_2exp_IF":
        popt, pcov = curve_fit(mod_2exp_IF, xdata= df["time"], ydata=df["diff_norm"], p0=p0, bounds=([0,0,0,0], [1,1,np.inf,np.inf]), maxfev=5000)
        xn = np.linspace(0, 600, 600) 
        yn = mod_2exp_IF(xn, *popt) #also this had to be inverted!!
        r2 = r_squared(df, popt, model) #also this had to be inverted in the r_squared function
    else:
        r2 = None
    return popt, pcov, xn, yn, r2 #return fitting parameters, x and y arrays for plotting and r_squared to store in single variable


# plotting function to plot all 4 fits next to each other with the residuals below
def plot(df, params_mod_noIF, params_mod_IF, params_mod_2exp, params_mod_2exp_IF, path, cellID, save, start):
    #set figure grid with subplots and their respective sizes
    fig, axs = plt.subplots(nrows=5, ncols=1,figsize=(5, 10), gridspec_kw={'height_ratios': [5,1,1,1,1]})
    
    #plot data and all 4 fits:
    axs[0].plot(df["time"], df["diff_norm"], 'o', color = 'black', alpha = 0.3)
    axs[0].plot(params_mod_noIF[2], params_mod_noIF[3], color = 'black', label = 'no IF, R\u00b2: ' + str(round(params_mod_noIF[4],3)))
    axs[0].plot(params_mod_IF[2], params_mod_IF[3], color = 'peru', label = 'IF, R\u00b2: ' + str(round(params_mod_IF[4],3)))
    axs[0].plot(params_mod_2exp[2], params_mod_2exp[3], color = 'blue', label = '2exp no IF, R\u00b2: ' + str(round(params_mod_2exp[4],3)))
    axs[0].plot(params_mod_2exp_IF[2], params_mod_2exp_IF[3], color = 'mediumorchid', label = '2exp IF, R\u00b2: ' + str(round(params_mod_2exp_IF[4],3)))
    axs[0].set_xlim([-10, df["time"].iloc[-1]+10])
    axs[0].set_ylim([0, 1.1])
    axs[0].legend()
    
    #plot residuals for first model
    axs[1].plot(df["time"], df["residuals_mod_noIF"], color = 'black')
    axs[1].axhline(0, color = "black")
    axs[1].set_xlim([0, df["time"].iloc[-1]])
    axs[1].set_ylim([-0.15, 0.15])
    
    #plot residuals for second model
    axs[2].plot(df["time"], df["residuals_mod_IF"], color = 'peru')
    axs[2].axhline(0, color = "black")
    axs[2].set_xlim([0, df["time"].iloc[-1]])
    axs[2].set_ylim([-0.15, 0.15])
    
    #plot residuals for third model
    axs[3].plot(df["time"], df["residuals_mod_2exp"], color = 'blue')
    axs[3].axhline(0, color = "black")
    axs[3].set_xlim([0, df["time"].iloc[-1]])
    axs[3].set_ylim([-0.15, 0.15])
    
    #plot residuals for fourth model
    axs[4].plot(df["time"], df["residuals_mod_2exp_IF"], color = 'mediumorchid')
    axs[4].axhline(0, color = "black")
    axs[4].set_xlim([0, df["time"].iloc[-1]])
    axs[4].set_ylim([-0.15, 0.15]) 
    # print(cellID)
    if save == True:
        plt.savefig(str(path)+'\\'+str(cellID) +'start=' +str(start) +'.png', dpi=500, format="png")
    else:
        pass

#define function to save parameters in csv file:
def save_params(cell_IDs, nuc_vols, params_mod_noIF, params_mod_IF, params_mod_2exp, params_mod_2exp_IF, bound_fractions, path, save, start): 
        params_df = pd.DataFrame() #creating empty dataframe in which all the data will be stored in later
        #creating empty lists, they will be fit with the respective values and each represent a column in the final dataframe
        cell_list = list()
        nuc_vol_list = list()
        model_list = list()
        r_squared_list = list()
        tau_list = list()
        fract_short_list = list()
        tau_long_list = list()
        fract_long_list = list()
        IF_list = list()
        bound_fraction_list = list()
        
        # len(params_mod_noIF) tells how many time-series were fitted, 
        # for loop allows to iterate through every single of those fitted time-series and append their parameters into the lists
        for i in range(0,len(params_mod_noIF),1):
            
            #add info for mod_noIF:
            cell_list.append(cell_IDs[i])
            nuc_vol_list.append(nuc_vols[i])
            model_list.append("mod_noIF")
            r_squared_list.append(params_mod_noIF[i][4]) #i specifies the time-series, [4] specifies that r_squared is the 5th element within the params_list of this time-series
            tau_list.append(params_mod_noIF[i][0][0]) #fitting params are stored in popt which is part of the params_list generated for each time-series, therefore [i] specifies time-series, [0] the location of popt in the params_list, and the second [0] the location of the fitting parameter of interest as defined in intial start parameters variable p[0]
            fract_short_list.append(float('nan')) #when 'nan' is appended, this parameter was not determined in this model fit
            tau_long_list.append(float('nan'))
            fract_long_list.append(float('nan'))
            IF_list.append(float('nan'))
            bound_fraction_list.append(bound_fractions[i])
            
            #info for mod_IF:
            cell_list.append(cell_IDs[i])
            nuc_vol_list.append(nuc_vols[i])
            model_list.append("mod_IF")
            r_squared_list.append(params_mod_IF[i][4])
            tau_list.append(params_mod_IF[i][0][1])
            fract_short_list.append(float('nan'))
            tau_long_list.append(float('nan'))
            fract_long_list.append(float('nan'))
            IF_list.append(params_mod_IF[i][0][0])
            bound_fraction_list.append(bound_fractions[i])
            
            #info for mod_2exp:
            cell_list.append(cell_IDs[i])
            nuc_vol_list.append(nuc_vols[i])
            model_list.append("mod_2exp")
            r_squared_list.append(params_mod_2exp[i][4])
            tau_list.append(params_mod_2exp[i][0][1])
            fract_short_list.append(params_mod_2exp[i][0][0])
            tau_long_list.append(params_mod_2exp[i][0][2])
            fract_long_list.append(1-params_mod_2exp[i][0][0])
            IF_list.append(float('nan'))
            bound_fraction_list.append(bound_fractions[i])
            
            #info for mod_2exp_IF:
            cell_list.append(cell_IDs[i])
            nuc_vol_list.append(nuc_vols[i])
            model_list.append("mod_2exp_IF")
            r_squared_list.append(params_mod_2exp_IF[i][4])
            IF_list.append(params_mod_2exp_IF[i][0][0])
            bound_fraction_list.append(bound_fractions[i])
            if params_mod_2exp_IF[i][0][2] > params_mod_2exp_IF[i][0][3]:
                tau_list.append(params_mod_2exp_IF[i][0][3])
                fract_short_list.append(1-(params_mod_2exp_IF[i][0][0]+params_mod_2exp_IF[i][0][1]))
                tau_long_list.append(params_mod_2exp_IF[i][0][2])
                fract_long_list.append(params_mod_2exp_IF[i][0][1])
            else:
                tau_list.append(params_mod_2exp_IF[i][0][2])
                fract_short_list.append(params_mod_2exp_IF[i][0][1])
                tau_long_list.append(params_mod_2exp_IF[i][0][3])
                fract_long_list.append(1-(params_mod_2exp_IF[i][0][0]+params_mod_2exp_IF[i][0][1]))
            
        
        # set lists as columns of previously defined empty dataframe
        params_df["cell"] = cell_list
        params_df["chr_volume_mic3"] = nuc_vol_list
        params_df['model'] = model_list
        params_df['r_squared'] = r_squared_list
        params_df['tau'] = tau_list
        params_df['fract_short'] = fract_short_list
        params_df['tau_long'] = tau_long_list
        params_df['fract_long'] = fract_long_list
        params_df['IF'] = IF_list
        params_df['bound_fract'] = bound_fraction_list
        #save dataframe as csv
        if save == True:
            params_df.to_csv(os.path.join(path + '\\' + 'fitting_results_' + 'start=' + str(start) + '.csv'), index=False)
        else:
            pass
        return params_df

# Main function that fits all models to the same time-series, plots and stores the outputs next to each other:
# (carries out out the data subsetting, fitting, plotting and saving)
def test_model_fits(df_bl, df_unbl, path, start = 0, save = False, plotting = False):
    #initiate lists for storage of parameters:
    cell_IDs = list()
    nuc_vols = list()    
    params_list_mod_noIF = list()
    params_list_mod_IF = list()
    params_list_mod_2exp = list()
    params_list_mod_2exp_IF = list()
    bound_fraction_list = list()
    #these lists will store the fitting parameters of every single time-series.
    
    #iterate through the individual time-series:
    for CellID in df_bl["cellID"].unique():
        
        #subset dataframe to current time-series
        current_df = subset_df(df_bl, CellID, start)
        #define initial fitting parameters
        p0_mod_noIF = 1 #only tau defined
        p0_mod_IF = [0,1] #immobile fraction and tau defined
        p0_mod_2exp = [0,10,1] #start params for fraction of long recovery time, tau_long and tau defined
        p0_mod_2exp_IF = [0, 0, 10, 1] #start params for IF, fraction of long recovery time, tau_long and tau defined
        
        #run fit and store results in params (contains popt, pcov, xn, yn, r2)
        params_mod_noIF = least_squares(p0_mod_noIF, current_df, "mod_noIF")
        params_mod_IF = least_squares(p0_mod_IF, current_df, "mod_IF") 
        params_mod_2exp = least_squares(p0_mod_2exp, current_df, "mod_2exp") 
        params_mod_2exp_IF = least_squares(p0_mod_2exp_IF, current_df, "mod_2exp_IF") 
        
        #calculate residuals
        current_df["residuals_mod_noIF"] = residual_mod_noIF(params_mod_noIF[0], current_df["time"], current_df["diff_norm"])
        current_df["residuals_mod_IF"] = residual_mod_IF(params_mod_IF[0], current_df["time"], current_df["diff_norm"])
        current_df["residuals_mod_2exp"] = residual_mod_2exp(params_mod_2exp[0], current_df["time"], current_df["diff_norm"]) 
        current_df["residuals_mod_2exp_IF"] = residual_mod_2exp_IF(params_mod_2exp_IF[0], current_df["time"], current_df["diff_norm"]) 

        #plot data and write parameters next to plots
        if plotting == True:
            plot(current_df, params_mod_noIF, params_mod_IF, params_mod_2exp,params_mod_2exp_IF, path, CellID, save, start)
        else:
            pass

        # calculate bound fraction using unbl and bl dataframe:
        # subset bl and unbl dataframe using the CellID (time-series) without any cropping
        df_bl_subset = df_bl.loc[df_bl["cellID"]==CellID]
        df_bl_subset = df_bl_subset.reset_index(drop=True)

        df_unbl_subset = df_unbl.loc[df_unbl["cellID"]==CellID]
        df_unbl_subset = df_unbl_subset.reset_index(drop=True)
        # append bound fraction to list and store in dataframe using saving function
        bound_fraction = bound_fract_corr_cell(df_unbl_subset, df_bl_subset)

        #append params to respective lists:
        cell_IDs.append(CellID)
        nuc_vols.append(current_df['chr_volume_mic3'][0])
        params_list_mod_noIF.append(params_mod_noIF)
        params_list_mod_IF.append(params_mod_IF)
        params_list_mod_2exp.append(params_mod_2exp)
        params_list_mod_2exp_IF.append(params_mod_2exp_IF)
        bound_fraction_list.append(bound_fraction)

    #save fit parameters to file:
    save_params(cell_IDs, nuc_vols, params_list_mod_noIF, params_list_mod_IF, params_list_mod_2exp, params_list_mod_2exp_IF, bound_fraction_list, path, save, start)
