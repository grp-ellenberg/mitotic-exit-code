# 05 Code for Analysis of FRAP data (half-nuclear photobleaching)

### FRAP data was analyzed in a 3-step procedure:
1) Image Analysis using Fiji-Jython (based on [Politi et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018/-/tree/master/FRAP?ref_type=heads))
2) Data processing using R (based on [Politi et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018/-/tree/master/FRAP?ref_type=heads))
3) Data Analysis using Python, including fitting of recovery data and data visualization

### Processed FRAP image data is deposted [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/05_FRAP?ref_type=heads).\

## Image Analysis
Image Analysis is performed using a custom written Fiji Jython script.

### Installation and usage
* Install FiJi
* Drag and drop [FRAP_analysis_AB_v0.0.py](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Code/05_FRAP?ref_type=heads) on the FiJi window, this should cause the opening of an editor
* Press Run
* The program will prompt for a FRAP database file. Select one of the FRAP database files provided [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/05_FRAP/01_file_list_image_processing?ref_type=heads) and make sure that it points to the raw data which can be accessed via [S-BIAD1454](https://www.ebi.ac.uk/biostudies/bioimages/studies/S-BIAD1454). The format of the file is a tab delimited file which gives the path to the images (one row per image).

## Data Processing

* Open [R-script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/05_FRAP/02_data_processing/MAIN_FRAP_analysis.Rmd?ref_type=heads) and run package loads and initialize processing functions.
* Then load database file that was also used for image analysis; make sure image analysis output is stored in the same folder as the database file.
* A results file for the bleached and unbleached region will be generated in the desired directory. The csv file containing the bleached chromatin area information provides the information of the normalized fluorescent recovery and can be used to visualize and fit the fluorescent intensity recovery. The csv file containing the unbleached chromatin area information is required to calculate chromatin-bound fractions (only after nuclear envelope reformation).

## Data Analysis, Fitting, Visualization

* provides functions to visualize and fit the fluroescent recovery data
* provides functions to analyze and plot the fitted FRAP data
* enables the generation of all FRAP-related figures of the manuscript.
