# Overview of Image Analysis script

This Fiji Jython script is used for Image Analysis of FRAP data with .czi or .tif format. It is based on [Politi et al. 2018](https://git.embl.de/grp-ellenberg/condensin_map_walther_jcb_2018/-/tree/master/FRAP?ref_type=heads) and has been updated to process also earlyG1 and G1 imaging data.\
\
In the following, an overview of the script is provided.

## Defining settings and functions:
1) import dependencies and set global constraints
2) define function to process data from early G1 cells: identifies top and bottom nucleus, extracts their centroid-centroid distance and saves them separately. Crops time-series to top nucleus to only analyze that one, returns cropped image to proceed with regular analysis
3) define function to segment nuclei (not used in early G1 function)
4) define mergeROIs function: checks if there are multiple rois and merges them 
5) define function to create lineROIs
6) define function to make filenames based on image name
7) define writeoutput function 

## Main function *run*:
1) initiate ROI manager
2) import image and sanity checks of image
3) check for cell cycle stage - if early G1: initiate early G1 function to crop image and determine early G1 timing
4) duplicate selected z-slices and prepare for processing
5) segment chromatin, adds as ROIs
6) calls createlineROIs function to make lineROIs by fitting an ellipse
7) perform refined segmentation
8) run anisotropic diffusion filter to remove random noise outside the object
9) call getfilenames function to prepare output files 
10) call writeoutput function to collect data and write to output files 
11) save ROIs and refined mask stack

## Initiation of whole script
1) *if __name__ == "__main__":* #initiates script if "Run" is pressed
2) lets you select database file with image path and additional parameters & info
3) generate a reader object that reads every row of file
4) iterates through readerobject to start analysis on one image after the other
