"""
FRAP_analysis_AB
ImageJ script to extract intensity profile of chromatin and a protein of interest along mitotic chromosomes. Modified from frapmitoticchr from Antonio Politi.
Filepaths to process and certain options are read from a database file.
The workflow is:
	Determine cell cycle stage (2 nuclei (=eG1) or metaphase plate) and select nucleus for analysis
    Segment chromatin per frame (Gaussian blur, Thresholding using one of the ImageJ methods (Default or Huang work well, fill holes).
    Compute 3D connected components and remove small objects.
    Find main axis of chromatin in each frame and create line roi (several pixels wide) along main axis.
    Optionally apply anisotropic diffusion filter.
    Apply binary mask to chromatin and POI.
    Extract data using the line ROI.
    Save profile data to file.
"""

# Some global constants. Change accordingly
#CHPOI = 1       # Channel with the protein of interest
CHMARKER = 2    # Channel with the cellular marker (e.g. DNA)
#NSLICES = [1,9] # start and end z-slice to process
#ANIDIFF = 1     # 0 or 1 depending if anisotropic diffusion filter has been applied
#FILTER_SIZE = 3 # filterer of gaussian blur

from ij import IJ
from ij.plugin import ChannelSplitter
from ij.plugin import Duplicator
dp = Duplicator()
from ij.plugin.frame import RoiManager
from ij.measure import Measurements #required for lineROI creation
from ij.gui  import Line, WaitForUserDialog
from ij.plugin import ImageCalculator
from ij.io import OpenDialog
from loci.plugins import BF #loci.plugins is the Bio-Formats plugin package, BF = Bio-Formats
from loci.plugins.in import ImporterOptions
import os
import csv
from java.awt import Color #to color the lineROIs
from java.lang import Double, Math #required for lineROI creation
from jarray import zeros #also for metadata extraction
from ij.measure import ResultsTable #for analyzing the 2D centroid distance of earlyG1 daughter cells
import math #to calculate the 2D centroid distance
from ij.io import FileInfo #to extract the pixel size in microns

def process_earlyG1(img, nSlices, nFrames, imgpath):
	#calculate id of middle slice
	slices_diff = nSlices[1] - nSlices[0] #to get difference between first and last slice. 
	slice_offset = slices_diff/2
	middle_slice = nSlices[0] + slice_offset
	## segment nuclei and extract centroids
	imc = dp.run(img, 2,2, middle_slice, middle_slice ,nFrames[0],nFrames[0]) #2,2 defines that I want the second channel (SiR-Hoechst), the middle slice of the slice selection and only the first image frame 
	imc.show()
	IJ.run(imc, "Smooth", "stack")
	imc.show()
	IJ.run(imc, "3D Objects Counter", "threshold=3000 slice=1 min.=500 max.=40000 exclude_objects_on_edges centroids statistics"); #run thresholding and measure centroids of nuclei, put down to 500 to recognized very early G1 cell
	IJ.renameResults("Results") ## renaming the statistics window is required such that it can be accessed
	IJ.selectWindow("Results")
	
	# get indices of nuclei
	table = ResultsTable.getResultsTable()
	tablesize = table.size()

	nuc_size_list = list()
	for i in (0,tablesize-1):
		nuc_size_list.append(table.getValue("Nb of obj. voxels",i))
	
	#select biggest two nuclei -> get their indices
	max_nuc1_idx = nuc_size_list.index(max(nuc_size_list))
	nuc_size_list[max_nuc1_idx] = 1 #set the biggest nucleus to a small voxel number value so that second biggest nucleus can be detected with same operation
	max_nuc2_idx = nuc_size_list.index(max(nuc_size_list))

	#get 2D centroid distance of the two nuclei
	#get centroid coordinates for biggest nucleus
	max_nuc1_X = table.getValue("X",max_nuc1_idx)
	max_nuc1_Y = table.getValue("Y",max_nuc1_idx)
	
	#get centroid coordinates for second biggest nucleus
	max_nuc2_X = table.getValue("X",max_nuc2_idx)
	max_nuc2_Y = table.getValue("Y",max_nuc2_idx)
	
	#calculate 2D centroid distance
	centroid_dist = math.hypot(max_nuc1_X - max_nuc2_X, max_nuc1_Y - max_nuc2_Y) #math.dist() does not work, maybe did not exist in python 2.7 that is used in jython?
	
	#get pixelsize in x (same as in y) to convert centroid distance in pixels to distance in microns:
	my_file_info = img.getFileInfo()
	pixelWidth = my_file_info.pixelWidth
	centroid_dist_mic = centroid_dist * pixelWidth
	#print (centroid_dist, centroid_dist_mic)
	
	#get the top nucleus of the two biggest nuclei:
	nuc1_Y = table.getValue("Y",max_nuc1_idx)
	nuc2_Y = table.getValue("Y",max_nuc2_idx)
	
	if nuc1_Y < nuc2_Y:
		top_nuc_idx = max_nuc1_idx
	if nuc2_Y < nuc1_Y:
		top_nuc_idx = max_nuc2_idx
	#print(top_nuc_idx)
	
	#top_nuc_idx is the top nucleus of the two earlyG1 nuclei in the image.
	
	#get surface area of top nucleus
	area_nuc1 = table.getValue("Surface (micron^2)",top_nuc_idx)
	#write centroid distances and surface area of top nucleus to file
	basename = os.path.splitext(os.path.basename(imgpath))[0]
	file_name = os.path.join(wd, "%s_G1_parameters.txt" % (basename))
	fid = open(file_name, 'w') #creates a file as defined by fnames, I guess fid stands for file ID
	fid.write("centroid_dist" + '\t' + "centroid_dist_mic" + '\t'+ 'nuc_area_mic' + '\n')
	fid.write(str(centroid_dist) + '\t' + str(centroid_dist_mic) + '\t' + str(area_nuc1))
	fid.close()
	
	#get centroid 
	top_nuc_X = table.getValue("X", top_nuc_idx)
	top_nuc_Y = table.getValue("Y", top_nuc_idx)
	#print(top_nuc_Y)
	
	#actually it would be sufficient to just crop the image in Y to exclude the other nucleus:
	#defining Y-limits:
	top_nuc_Y = round(top_nuc_Y)
	crop_Y1 = top_nuc_Y -300 #I just subtracted a very high amount to ensure that the top of the image is not cropped
	if crop_Y1 < 0:
		crop_Y1 = 0
	crop_Y2 = top_nuc_Y + 40
	#print(crop_Y1, crop_Y2)
	
	#defining x limits do not crop in x:
	x0 = 0
	x1 = img.getWidth()
	
	img.setRoi(x0,int(crop_Y1),x1, int(crop_Y2)) #region to be duplicated is set
	cropped_image = dp.run(img)
	cropped_image.show()
	
	return (cropped_image)

def segment(imgM, cell_cycle_stage, thr_method = "Huang", filter_size = 3):
    """
    segment single channel stack
    :param imgM: image plus single channel
    :param thr_method: method used to compute threshold
    :param filter_size: size of sigma for Gaussian blut
    :return: binary of segmented img. image plus
    """
    IJ.run(imgM, "Gaussian Blur...", "sigma=%d stack" %filter_size)
    if cell_cycle_stage == 'interphase':
    	IJ.setAutoThreshold(imgM, "%s dark" % "Huang")
    	IJ.run(imgM, "Convert to Mask", "method=Huang background=Default calculate")
    else:
    	IJ.setAutoThreshold(imgM, "%s dark" % thr_method)
    	IJ.run(imgM, "Convert to Mask", "method=Default background=Default calculate") 
    IJ.run(imgM, "Invert", "stack");
    IJ.run(imgM, "Fill Holes", "stack")
    IJ.run(imgM, "Invert", "stack");
    imgM.show()
    return(imgM)
#

def mergeRois(img, rois):
    """
    For each slice of image check if there are multipe rois and merge them ##note Andi: maybe required to connect multiple big chromatin masses that are not connected but stem from the same metaphase plate? might need to be modified for early G1 analysis. 
    ##this code also makes the array of ROIs a list to iterate through.
    :param img : image plus of Chromatin or POI
    :param rois: an array of rois
    """
    roim = RoiManager.getInstance()
    if roim is None:
        roim = RoiManager()
    roim.runCommand('reset')
    roiCleanup = []
    for islice in range(1, img.getNSlices()+1):
        roislice = []
        img.setSlice(islice)
        for iroi, roi in enumerate(rois):
            if roi.getPosition() == islice:
                roislice.append(roi)
        if len(roislice) == 0:
            continue
        if len(roislice) > 1:
            for roi in roislice:
                roim.addRoi(roi)
            roim.setSelectedIndexes(range(0, len(roislice)))
            roim.runCommand("Combine")
            roim.runCommand('Add')
            roim.select(len(roislice))
            roi = roim.getRoi(len(roislice))
        else:
            roi = roislice[0]
        roi.setPosition(islice)
        roiCleanup.append(roi)
        roim.runCommand('reset')  
         
    for roi in roiCleanup:
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
        roim.addRoi(roi) 
    
    return(roiCleanup)

def createLineRois(img, rois, length = 90.0, width = 5):
    '''
    Create a line roi using the principal component of 2D fitted ellipse to a polyline roi
    :param img: an image associated to the roi
    :param rois: a list of rois
    :param length: maximal length of line in pixels
    :param width: width of line in pixels
    :return: list of line rois
    '''
    roiLines = list()
    halflength = length/2.0
#    print(rois)
    for iroi, roi in enumerate(rois):
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
#        img.show()
        ell = img.getStatistics(Measurements.ELLIPSE, Measurements.CENTROID)
        anglerad = Math.toRadians(ell.angle)
        #print(anglerad)
        startCoord = [ell.xCentroid - halflength*Math.cos(anglerad), ell.yCentroid + halflength*Math.sin(anglerad)]
        endCoord = [ell.xCentroid +  halflength*Math.cos(anglerad), ell.yCentroid -  halflength*Math.sin(anglerad)]
        # avoid flipping of reading direction of line profile
        if (anglerad < Math.PI/2):
            endCoord_old = endCoord           
            endCoord = startCoord
            startCoord = endCoord_old
        lroi = Line(round(endCoord[0]), round(endCoord[1]), round(startCoord[0]),round(startCoord[1]))
        lroi.setStrokeWidth(width)
        lroi.setPosition(roi.getPosition())
        roiLines.append(lroi)
#        print(roiLines)

    return(roiLines)

def getfilenames(imgpath, aniDiff):
    """
    Create a list of file names
    :param imgpath: the path to the image
    :param aniDiff: 0 or 1 if anistropic postfix is added to file name
    :return: list of file names
    """
    wd = os.path.dirname(databasefile) # replace databasefile by (imgpath) to store ROI data in same folder as images
    #try:
    #    os.mkdir(os.path.join(os.path.dirname(wd), 'Results'))
    #except OSError as exc:
    #    if exc.errno != errno.EEXIST:
    #        raise
    pass

    if aniDiff:
        addtxt = 'aniDiff'
    else:
        addtxt = ''
        
    basename = os.path.splitext(os.path.basename(imgpath))[0]
#    print(basename)
    file_names = [os.path.join(wd,  '%s_%s_poi.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_chr.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_mask.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_poi_bg.txt' % (basename, addtxt)),
                  os.path.join(wd,  '%s_%s_inverted_mask.txt' % (basename, addtxt)),
                  os.path.join(wd, '%s_%s_poi.tif' % (basename, addtxt)), 
                  os.path.join(wd, '%s_%s_poi_bg.tif' % (basename, addtxt)), 
                  os.path.join(wd, '%s_%s_roi.zip' % (basename, addtxt))]
    return(file_names)

def writeoutput(imgs, rois, fnames):
    """
    Write output of intensity in roi pixels
    :param imgs: a list of images
    :param rois: a list of rois
    :param fnames: a list of output paths
    :return: None
    """
    # max length of output vector
    #print(rois)
#    imgs[0].show()
    imgs[0].setRoi(rois[0]) #imgs[0] is imgP (protein information), rois are the lineROIs, sets the first lineROI on the POI image
    maxL = len(imgs[0].getRoi().getPixels().tolist()) #gets the number of pixels of the lineROI and sets it as maxL (in this case maxL is 91 because the lineROI is 90 pixels wide and 1 pixel high
#    print("maxL: "+ str(maxL))
# maxL is only required in the code line below that Antonio commented out originally (fid.write('slice\ttpos...)') -> I replaced it with maxEffectiveLength +1 because it then yields the right number of column descriptors.
    # maximum nu
    NSlices = imgs[0].getNSlices() #gets the number of slices of the image (=the number of z-stacks which is in this case: 1)
    lLines = list() #to store the length of individual lineROIs
    for iroi, roi in enumerate(rois): #iterate through ROIs to
        imgs[0].setRoi(roi) #1) set the ROI to the image, probably it does not matter whether it is set to frame 1 or 2 or 200
        lLines.append(len(imgs[0].getRoi().getPixels().tolist())) #2)get the length of the ROI and append it to the lLines list
    maxEffectiveLength = max(lLines) #gets the maximum length of a line, here it is 92.
    for iname, name in enumerate(fnames[0:5]): #iterates through enumerated files (3 in total), added 2 for inversion
        fid = open(name, 'w') #creates a file as defined by fnames, I guess fid stands for file ID
#        fid.write('slice\ttpos\ttime(s)\tzpos\t' + '\t'.join(['X%d' % p for p in range(1,maxEffectiveLength+1) ]) + '\n') #defines what is written in the first line of the file:
        # slice, tpos and zpos (all separated by a tab), then X1, X2, X3 etc for the pixels from X1 up to X90, because the end value of range(1,maxL) is not used anymore, there are only X1-X90 printed in the file
        # I could imagine that Antonio commented that out again because it makes subsequent processing of the files easier
        # I do not really get why he did not use maxEffectiveLength instead of maxL, maybe because he anyway abandoned it later?
        for iroi, roi in enumerate(rois): #iterates through enumerated ROIs
            imgs[iname].setSlice(roi.getPosition()) #takes image (either POI, Ch or refined segmented image) and sets the corresponding slice
            imgs[iname].setRoi(roi) #sets the ROI on the image
            fid.write('%d\t%d\t%d\t' % (roi.getPosition(),  Math.floor((roi.getPosition()-1)/NSlices)+1, ((roi.getPosition()-1) % NSlices) + 1))
            # writes the ROIposition (slice), the frame (tpos), the timestamps and the z-slice to the file, all separated by a tab, %d is a placeholder for a decimal integer, %f for a float
            outstr = '\t'.join(['%.3f' % el for el in imgs[iname].getRoi().getPixels().tolist()]) #collects all pixel values along the lineROI in the list
            lString = len(imgs[iname].getRoi().getPixels().tolist()) #gets the length of this list... why not take len(outstr)->because outstr is a string and len(outstr) would return the number of characters in it
            if (lString < maxEffectiveLength):
                dL = maxEffectiveLength - lString #dL is the difference in length
                if dL % 2 == 0: #if the difference is an even number: puts the required amount of zeros on the left and right   
                    outstr = '\t'.join(['%.3f' % 0  for i in range(1,dL/2+1)]) +  '\t' + outstr #puts half of the zeros on the left
                    outstr = outstr + '\t' +  '\t'.join(['%.3f' % 0 for i in range(1,dL/2+1)]) # and half on the right
                else:
                    dL = dL+1 #if dL is uneven, one more zero is put on the left compared to the right.
                    outstr = '\t'.join(['%.3f' % 0 for i in range(1,dL/2+1)]) +  '\t' + outstr
                    outstr = outstr +  '\t' + '\t'.join(['%.3f' % 0 for i in range(1,dL/2)])
            fid.write(outstr + '\n') #writes final outstr to file as a complete line.
        fid.close()

def run(imgpath, ChPOI, ChMarker, nSlices, nFrames, n_timepoints, cell_cycle_stage, aniDiff = True, thr_method1 = 'Huang', filter_size = 3, exclude = 1): #turn nFrames to [1,1] if processing of only one timepoint is required.
    """
    run pipeline
    :param imgpath: path to image
    :param ChPOI: Channel of POI
    :param ChMarker: Channel of marker where to perform segmentation
    :param nSlices: [z_slice_start, z_slice_end]
    :param nFrames: [t_frame_start, t_frame_end]
    :param aniDiff: boolean for applying anisotropic 2D filter before extracting itensities
    :param filter_size: size of Gaussian filter in segment function
    :param exclude: boolean to exclude objects touching the boundary in the first round
    """
    
    # Sanity checks to check whether we have more than one frame and more than one z-slice, I will comment that out for now since i am working with an image that has 200 frames, but only 1 z-slice
#    assert nFrames[0] < nFrames[1], 'Number of frames to investigate nFrames must be [frame_start, frame_end] with frame_start < frame_end'
#    assert nSlices[0] < nSlices[1], 'Number of slices to investigate nFrames must be [slice_start, slice_end] with slice_start < slice_end'
#    print("from slice " +str(nSlices[0]))
#    print("to slice " + str(nSlices[1]))
#    print(cell_cycle_stage)
    IJ.run("Close All", "")

    # initiate RoiManager and other classes needed
    roim = RoiManager.getInstance()
    if roim is None:
        roim = RoiManager()
    ic = ImageCalculator()

    # import image
    opt = ImporterOptions()
    opt.setVirtual(True)
    opt.setId(imgpath)
    img = BF.openImagePlus(opt)[0]
#    imp = IJ.openImage(imgpath) #only works for me when I directly enter the image path
#    img.show() #does only work well when I put img, not when putting imp

    # further sanity checks 
    if nFrames[0] < 1: #checks if first frame is set as 1
        nFrames[0] = 1
        print("nFrames is lower than 1")
    if nFrames[1] > img.getNFrames(): #nFrames[1] represents the last frame, adjusts nFrames if it is higher than the actual number of frames
        nFrames[1] = img.getNFrames()
        print("nFrames is higher than the frame number of the image")
    if nSlices[0] < 1:
        nSlices[0] = 1
        print("the number of lower slices has to be 1 or higher")
    if nSlices[1] > img.getNSlices(): #same for the z-slices
        nSlices[1] = img.getNSlices()
        print("the number of top slice has to be lower or equal to the max number of slices")

    cell_cycle_stage = cell_cycle_stage
    if cell_cycle_stage == "earlyG1":
#        print("PROPERLY INITIATED")
        img = process_earlyG1(img, nSlices, nFrames, imgpath)
    if cell_cycle_stage != "earlyG1":
        pass

    img = dp.run(img, 1, 2, nSlices[0], nSlices[1], nFrames[0], nFrames[1]) #duplication of image to get rid of transmission channel (3rd one)
#    img.show()

    # remove scale ### why is that required?
    IJ.run(img, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")
    
    # split channels
    imgC = ChannelSplitter.split(img) #imgC is an array of the two split images
#    imgC[0].show() #0 for GFP channel, 1 for Sir-Hoechst

#    # flatten dimensions for convenience
    for cimg in imgC:
       IJ.run(cimg, "Hyperstack to Stack", "")
#       cimg.show()
#    imgC[0].show()


#    # segment DNA mass. This will be used to find axis of chromatin per frame
#    # Objective is one ROI per main chromatin mass per slice
#    # The Huang filter oversegments DNA but avoid discontinuities in Rois.
#    # Typically Default filter works fine
#
    imgM = segment(imgC[ChMarker-1].duplicate(), cell_cycle_stage, thr_method = thr_method1, filter_size = filter_size)
#    imgM.show()
##
#    # computed 3D connected components and remove small objects
#    imgM.show()
    IJ.run("Connected Components Labeling", "connectivity=6 type=[16 bits]")
    IJ.run("Keep Largest Region", "")
    imgM = IJ.getImage()
    imgM.setTitle('Segmented_3Dblob') #while the original duplicated image had 200 frames, these frames are now converted to z-slices in the segmented 3D blob image.
#    IJ.run(imgM, "Invert", "stack"); #I added this because for some reason the analyze particles function does either recognize the entire image as ROI (when exclude on edges is de-selected) or does not work at all (when exclude on edges is activated)
#    imgM.show()
    # remove remaining single small blobs and create roi table and line ROIs
    IJ.run("Set Measurements...", "area center fit shape integrated stack redirect=None decimal=5") #sets measurements to area, center of mass, fit elipse etc. etc.
    roim.runCommand("reset")
    if exclude: #refers to the "exclude" parameter in the function definition of the run function, seems to be set "1" as default
        IJ.run(imgM, "Analyze Particles...", "size=200-Infinity pixel show=Masks clear exclude add in_situ stack") 
    else:
        IJ.run(imgM, "Analyze Particles...", "size=200-Infinity pixel show=Masks clear add in_situ stack")
    roiMarker = roim.getRoisAsArray()
#    print(roiMarker)
    roiMarker = mergeRois(imgM, roiMarker) #mergeROIs is a function defined by Antonio to merge remaining ROIS and convert the roiMarker array to a list, see above
    roim.runCommand('reset')
    for roi in roiMarker: #iterates through all ROIs and sets the individual ROIS on the original duplicated image, not sure what the purpose of that is - is it required to create lineROIs?
        img.setSlice(roi.getPosition())
        img.setRoi(roi)
        roim.addRoi(roi) #not sure why he adds the ROIs to the manager and removes them again in the next step
    roim.runCommand('reset')
    if cell_cycle_stage == "earlyG1":
       roiLines = createLineRois(img = imgM,  rois = roiMarker, length = 90.0, width = 40)
    if cell_cycle_stage == "metaphase":
       roiLines = createLineRois(img = imgM,  rois = roiMarker, length = 90.0, width = 45 ) #set to 45 after testing background subtraction procedure on my metaphase data.
    if cell_cycle_stage == "interphase":
       roiLines = createLineRois(img = imgM,  rois = roiMarker, length = 90.0, width = 55 ) 
#    print(roiLines)


    # Perform a refined segmentation of DNA mass using Otsu threshold and less smoothing
#    print("refined segmentation")
    imgM = segment(imgC[ChMarker-1].duplicate(), cell_cycle_stage, thr_method = 'Otsu', filter_size = 2)
    IJ.run(imgM, "Analyze Particles...", "size=100-Infinity pixel show=Masks exclude clear add in_situ stack")
    IJ.run(imgM, "Multiply...", "value=%f stack" % (1.0/255.0)) #make mask as it is used to crop out region of interest in POI and chr channel
    # this can have double Roi per object
    roiRefined = roim.getRoisAsArray()
    

    # Quantification of images
    # Anisotropic diffusion to eliminate some random noise outside object
    if aniDiff:
        IJ.run(imgC[ChPOI-1], "Anisotropic Diffusion 2D", "number=20 smoothings=1 a1=0.50 a2=0.90 dt=20 edge=5")
        imgP = IJ.getImage()
        imgP.hide()
        IJ.run(imgC[ChMarker-1], "Anisotropic Diffusion 2D", "number=20 smoothings=1 a1=0.50 a2=0.90 dt=20 edge=5")
        imgCh = IJ.getImage()
        imgCh.hide()
    else:
        imgP = imgC[ChPOI-1]
        imgCh = imgC[ChMarker-1]
#    # Mask the chromatin and POI - this is where the mask is imposed on the POI and Chr channels
#   duplicate imgP before the mask is imposed on it
    imgPI = imgP.duplicate()
    imgP = ic.run("Multiply create stack", imgM, imgP) #ic = ImageCalculator
    imgP.show() #here, the mask is already imposed on the POI channel
    imgCh = ic.run("Multiply create stack", imgM, imgCh)
#    imgCh.show()
    imgP.setTitle('POI')
    imgCh.setTitle('Chromatin')
    imgM.setTitle('Refined Mask')
    
#    make inversion of mask:
    imgI = imgM.duplicate() #needs .duplicate() to function properly
    IJ.run(imgI, "Invert", "stack")
    IJ.run(imgI, "Subtract...", "value=254 stack")
#    impose inverted mask on POI channel
    imgPI = ic.run("Multiply create stack", imgI, imgPI)
    imgP.show()
#    imgCh.show()
    imgPI.show()
#    imgM.show()
#    imgI.show()
    
#    imgM.show()
    roim.runCommand('reset')
    for iroi, roi in enumerate(roiLines):
        imgP.setSlice(roi.getPosition())
        #roim.add(imgP, roi, iroi) #not required (commented out by antonio)
        roi.setColor(Color(255, 0, 0, 125))
        imgP.setRoi(roi)
        roim.addRoi(roi)
#    # Generate output
    imgO = [imgP, imgCh, imgM, imgPI, imgI] #list of protein ROI images, chromatin ROI images and refined segmented image
    file_names = getfilenames(imgpath, aniDiff)
    writeoutput(imgO, rois = roiLines, fnames = file_names)
    IJ.saveAs(imgP, "Tiff", file_names[5])
    IJ.saveAs(imgPI, "Tiff", file_names[6])
    roim.runCommand("Save", file_names[7])
    imgM.show()
    roim.show()

if __name__ == "__main__": #initiates script if "Run" is pressed
    # Run pipeline
    od = OpenDialog('Select a FRAP database file', '', 'database_frap_AB.txt')
    databasefile = os.path.join(od.getDirectory(), od.getFileName())
    wd = os.path.dirname(databasefile)
    with open(databasefile, 'rU') as csvfile: #turns out python version 2.5 is implemented in Jython and the option newline='' cannot be used instead of the python3-deprecated 'rU'
        reader = csv.DictReader(csvfile, dialect = csv.excel_tab) #generates a reader object that reads every row of the csvfile
        allrows = []
        for row in reader: #put the rows in a list to iterate through them
            allrows.append(row)
    for irow, row in enumerate(allrows): #iterate through rows, every row is one FRAP time-series
        if row['manualQC'] == '1':
            endframe = int(row['processuptoframe'])
            print(os.path.join(wd, row['path'] + row['file_format'])) #'.czi' was changed to '.tif'
            if endframe == 0:
                endframe = 200 #use based on maximum time-series length of experiment
            run(os.path.join(wd, row['path'] + row['file_format']), ChPOI = int(row['ChPOI']), ChMarker = int(row['Chr_marker']), #generates the path of the image (actually it does not matter if parts of the paths overlap), gathers all additional parameters and the executes the run function with it.
                nSlices = [int(row['start_slice']), int(row['end_slice'])], nFrames = [1, endframe], n_timepoints = int(row['n_timepoints']), thr_method1 = row['method'], cell_cycle_stage = row['cell_cycle_stage'],
                aniDiff = True, filter_size = 3) #'.czi' was changed to '.tif'
#            wd = WaitForUserDialog('Press OK if cells if correct')
#            wd.show()
#            if wd.escPressed():
#                break

            