# 04 Code for Analysis of Spot-bleach data

Spot-bleach data per replicate is deposted [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/04_Spot_bleach/Replicate_data?ref_type=heads).\

Python Notebook performs the following tasks:
* Loading of replicate data
* Adjusting chromatin-bound fractions based on mEGFP (0% chromatin-bound) and H2B-EGFP (used as 100% chromatin bound control) data. H2B-EGFP was found to be >90% chromatin-bound based on half-nuclear photobleaching.
* Plotting of bound fractions per timepoint as displayed in paper. 