### This script defines general functions for the analysis of full cell cycle time-series data
### Author: Andreas Brunner, EMBL, Heidelberg
### Last update: 2023-02-06

#import all required libraries
import os
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# determine how many time-series are stored in dataframe of concatenated time-series of specific length
def number_of_time_series(df, N_intervals):
    total_rows = len(df)
    n_time_series = int(total_rows/N_intervals)
    return n_time_series


# function to open dataframe:
def read_df(path, datasheet, filesep):
    # os.getcwd()
    os.chdir(path)
    df = pd.read_csv(datasheet, sep = filesep)
    return df

# define name of dataframe
def name_df(df, POI_name_long, N_intervals):
    df.name = POI_name_long #+ " n=" + str(number_of_time_series(df, N_intervals))
    return df


# Annotate a cell number to each individual time-series as well as a time-value for each individual timepoint, starting with count = 1. 
# Variables are the dataframe of interest (df) and the common length of the time-series stored in the dataframe
def annotating_cells(df, N_intervals): 
    n_time_series = number_of_time_series(df, N_intervals) #get number of time-series in dataframe
    timepoints = list() #initiate list that will store all annotated timepoints
    cell_numbers = list() #initiate list that will store the cell number annotations
    cellcount = 1 #initiate cellcount variable 
    for element in range(n_time_series): #iterate through all time series
        timepoints.extend(range(N_intervals))   #extend list with timepoints by extending the list with the entire range from 0 -> n_time_series
        for interval in range(N_intervals): #iterate through each time-series and append celllist with the current cellcount value
            cell_numbers.append(cellcount)      
        cellcount += 1 #increment the cell count for the next cell to be assigned a cell number
        #print(timepoints)
    df["timepoint"] = timepoints #add timepoints stored in timepoints to dataframe
    df["Cell"] = cell_numbers    #add timepoints stored in cell_numbers to dataframe
    return df


# function to check all time-series of a dataframe un-aligned with a given parameter plotted:
def plot_time_series(df, time, what_to_plot, N_intervals): #function to plot all time series at in one plot, what_to_plot will indicate the y-value of interest
    df = df.loc[:, df.columns != 'File_path'].astype(float)
    sns.lineplot(x = time, y= what_to_plot, data = df, hue="Cell", legend = False)
    plt.xlabel(str(time))
    plt.ylabel(str(what_to_plot))

# function to plot cellular and nuclear value of interest for each time-series individually
def plot_individual_series(df, what_to_plot_cell, what_to_plot_nuc): 
    df = df.loc[:, df.columns != 'File_path'].astype(float)
    for cell,values in df.groupby("Cell"):
        fig, ax = plt.subplots()
        sns.lineplot(x = "timepoint", y= what_to_plot_cell, data = values, legend = False, color = "black", alpha = 0.8)
        plt.ylabel(str(what_to_plot_nuc) + " and " + str((what_to_plot_cell)))
        plt.xlabel("time series: " + str(cell))
        sns.lineplot(x = "timepoint", y= what_to_plot_nuc, data = values, legend = False)


# remove unwanted time-series based on list and check the remaining time-series again if wanted:
def remove_time_series(df, cells_to_keep, what_to_plot_cell, what_to_plot_nuc, POI_name_long, N_intervals):
    df_filtered = pd.DataFrame() #initiate new dataframe that shall be filled with the time-series that are left after filtering out faulty ones
    boolean_series = df.Cell.isin(cells_to_keep) #assign True/False to time-series that shall be kept/removed
    df_filtered = df[boolean_series] #add time-series that shall be kept to filtered dataframe
    df_filtered = df_filtered.reset_index(drop=True) 
    name_df(df_filtered, POI_name_long, N_intervals)
    
    return df_filtered


# crop out the cell cycle regions of interest:
def crop_to_cell_cycle(df, crop_cell_vol, crop_nuc_vol):
# this function crops out individual full cell cycles via a Crop flag. 
    CropFlag = False #CropFlag is initiated as False
    list_of_lists = list() #will contain the CropFlags of all time_series that are appended time-series by time-series through crop_list
    
    for name, group in df.groupby("Cell"): #iterate through time-series
        crop_list = list() #initiate a list that contains the cropping flags for all intervals of a single time-series
#         print(df.groupby('Cell').size()[name])
        for i in range(0, df.groupby('Cell').size()[name]-2): #iterate through intervals of a time series
            if group["Vol_cell_mic3"].iloc[i+2]<group["Vol_cell_mic3"].iloc[i]/crop_cell_vol and group["Vol_nuc_mic3"].iloc[i+1]<group["Vol_nuc_mic3"].iloc[i]/crop_nuc_vol and CropFlag == False:
            #if Vol_cell_mic3 at i+1 is 1.45x smaller than at interval i and the CropFlag is False, then the Flag is turned True to initiate the full cell cycle
                CropFlag = True
                crop_list.append(CropFlag) #CropFlag is appended every iteration
            elif group["Vol_cell_mic3"].iloc[i+2]<group["Vol_cell_mic3"].iloc[i]/crop_cell_vol and group["Vol_nuc_mic3"].iloc[i+1]<group["Vol_nuc_mic3"].iloc[i]/crop_nuc_vol and CropFlag == True:
            #if the volume decreases between i and i+1, but the CropFlag is already true (i.e. second division), the CropFlag is turned False again
                crop_list.append(CropFlag)
                CropFlag = False
            else:
                crop_list.append(CropFlag)
        crop_list.append(CropFlag)
        crop_list.append(CropFlag)
        list_of_lists.append(crop_list)
        
    flat_list = [item for sublist in list_of_lists for item in sublist] #flattens the list of lists to a single list
    df["CropFlag"] = flat_list
    #df.to_csv(df.name + "new.csv") #optonal to save csv for troubleshooting

    # set time of individual timeseries
    time = 0 # the variable time initiates at t=0 and is then incremented by timepoint
    timelist = list() # the timelist stores all times for all timepoints as a list and is later appended to the dataframe
    interval = 0 # the interval variable and list are used to enumerate all intervals within a cell cycle
    intervallist = list()
    cell_cycle_durations = list() # the cell_cycle_durations list collects all maximum timepoints per full cell cycle such that an average can be calculated later on
    #iterate through dataframe and add generate time(min) values for all timepoints within the individual cell cycles
    for i in range(len(df)):
        if (df["CropFlag"][i] == True) == True:
            timelist.append(time)
            time += 10
            intervallist.append(interval)
            interval +=1
        if (df["CropFlag"][i] == True) == False:
            if time > 0: #collect all cell cycle durations in one list to calculate average cell cycle duration later on
                cell_cycle_durations.append(time)
            time = 0
            interval = 0
            timelist.append(0)
            intervallist.append(0)
    # append times and enumerated intervals as new columns to the dataframe
    df["time(min)"]=timelist
    df["timepoint"]=intervallist

    df_cropped = df.loc[df['CropFlag'] == True] #crop dataframe based on CropFlag - only timepoints within full cell cycles are copied
    df_cropped = df_cropped.reset_index(drop=True) #reset index of dataframe
#     df_cropped.to_csv(df.name + ".csv") #save dataframe as .csv file to checkcell_cycle_durations = list()
    return df_cropped, cell_cycle_durations


# crop out the cell cycle regions of interest only based on cell volume information:
def crop_to_cell_cycle_cell_vol(df, crop_cell_vol):
# this function crops out individual full cell cycles via a Crop flag. 
    CropFlag = False #CropFlag is initiated as False
    list_of_lists = list() #will contain the CropFlags of all time_series that are appended time-series by time-series through crop_list
    
    for name, group in df.groupby("Cell"): #iterate through time-series
        crop_list = list() #initiate a list that contains the cropping flags for all intervals of a single time-series
#         print(df.groupby('Cell').size()[name])
        for i in range(0, df.groupby('Cell').size()[name]-1): #iterate through intervals of a time series
            #NaN check was implemented to allow for cropping even when NaN are in the dataframe (in the case of STAG2)
            if group["Vol_cell_mic3"].iloc[i] == 'NaN':
                crop_list.append(CropFlag)
            elif group["Vol_cell_mic3"].iloc[i+1] == 'NaN':
                crop_list.append(CropFlag)
            elif group["Vol_cell_mic3"].iloc[i+1]<group["Vol_cell_mic3"].iloc[i]/crop_cell_vol and CropFlag == False:
            #if Vol_cell_mic3 at i+1 is 1.45x smaller than at interval i and the CropFlag is False, then the Flag is turned True to initiate the full cell cycle
                CropFlag = True
                crop_list.append(CropFlag) #CropFlag is appended every iteration
            elif group["Vol_cell_mic3"].iloc[i+1]<group["Vol_cell_mic3"].iloc[i]/crop_cell_vol and CropFlag == True:
            #if the volume decreases between i and i+1, but the CropFlag is already true (i.e. second division), the CropFlag is turned False again
                # print(name, i, CropFlag)
                crop_list.append(CropFlag)
                CropFlag = False
                # print(name, i, CropFlag)
            else:
                crop_list.append(CropFlag)
        crop_list.append(CropFlag)
        list_of_lists.append(crop_list)
        
    flat_list = [item for sublist in list_of_lists for item in sublist] #flattens the list of lists to a single list
    df["CropFlag"] = flat_list
    # df.to_csv(df.name + "new.csv") #optonal to save csv for troubleshooting
    
    # set time of individual timeseries
    time = 0 # the variable time initiates at t=0 and is then incremented by timepoint
    timelist = list() # the timelist stores all times for all timepoints as a list and is later appended to the dataframe
    interval = 0 # the interval valiable and list are used to enumerate all intervals within a cell cycle
    intervallist = list()
    cell_cycle_durations = list() # the cell_cycle_durations list collects all maximum timepoints per full cell cycle such that an average can be calculated later on
    #iterate through dataframe and add generate time(min) values for all timepoints within the individual cell cycles
    for i in range(len(df)):
        if (df["CropFlag"][i] == True) == True:
            timelist.append(time)
            time += 10
            intervallist.append(interval)
            interval +=1
        if (df["CropFlag"][i] == True) == False:
            if time > 0: #collect all cell cycle durations in one list to calculate average cell cycle duration later on
                cell_cycle_durations.append(time)
            time = 0
            interval = 0
            timelist.append(0)
            intervallist.append(0)
    # append times and enumerated intervals as new columns to the dataframe
    df["time(min)"]=timelist
    df["timepoint"]=intervallist

    df_cropped = df.loc[df['CropFlag'] == True] #crop dataframe based on CropFlag - only timepoints within full cell cycles are copied
    df_cropped = df_cropped.reset_index(drop=True) #reset index of dataframe
#     df_cropped.to_csv(df.name + ".csv") #save dataframe as .csv file to checkcell_cycle_durations = list()
    return df_cropped, cell_cycle_durations


# Alignment of time-series by start and end in a pseudotime timeframe between 0 and 1
def align_cell_cycles(df):
    l = list() #initiate list in which individual new pseudotimes will be saved
    for name, group in df.groupby('Cell'): #iterate through individual cells
        group["pseudotime"] = group["timepoint"] *(1/(df.groupby('Cell').size()[name]-1)) #and calculate pseudotimes
        # by dividing each timepoint by the inverse of the total number of timepoints-1. this normalizes the pseudotime to 1
        # it does not really matter whether the value is assigned to group["pseudotime"] or something else, it exists anyway
        # only as a copy of the dataframe and has to be written into a list and appended to the real dataframe later on.
        # .groupby.size() can be used to infer the size of each individual group
        for element in group["pseudotime"]: #append pseudotimes to list
            l.append(element)
    df["pseudotime"] = l #add new column to the list
    return df

# interpolate and average the time-series:
def interpol_plot_avg_time_series(df, what_to_plot, color):
    # generate numpy array for x-coordinates that are the new timepoints for averaging
    x = np.linspace(0,1,100)

    #initiate empty list that will be filled successively with arrays
    lis_x = list() #will be appended with arrays containing the new x-values (always the same as defined above)
    lis_y = list() #will be appended with arrays containing the new y-values as spit out by y_f(x)
    cell_count = 1 #since a new df is being created and since it has a different size, the individual cells again need to be defined
    cell_counter_list = list()
    for name, element in df.groupby("Cell"): #iterate through time-series
        y_f = interp1d(element["pseudotime"], element[what_to_plot], "cubic", fill_value="extrapolate") #generate a function for every cell based on interpolation
        y = y_f(x) #calculate y for respective x value
        lis_x.append(x)
        lis_y.append(y)
        #add again a list with the respective identities (the old identities do not fit the new number of intervals)
        for i in range(len(x)):
            cell_counter_list.append(cell_count)
        cell_count += 1

# put pseudotime data into new dataframe
    df_pseudotime = pd.DataFrame()
    df_pseudotime["pseudotime"] = np.concatenate(lis_x).ravel().tolist()
    df_pseudotime[what_to_plot] = np.concatenate(lis_y).ravel().tolist()
    df_pseudotime["cell"] = cell_counter_list
    
# return df_pseudotime
    fig, ax = plt.subplots()
    sns.lineplot(x = "pseudotime", y= what_to_plot, data = df_pseudotime, color = color, errorbar = "sd")
    #ax.set_xlim(0,1)
    ax.set_ylim(bottom = 0)


def plot_average_time_series(df, time, plot, color):
    fig, ax = plt.subplots()
    sns.lineplot(x = str(time), y = plot, data = df, color = color, errorbar = "sd")
    ax.set_ylim(bottom = 0)


# interpolate and average a single time-series (to be used in function interpolate_time_series)
def interpolate(group, x, what_to_plot):
    lis_y = list()
    y_f = interp1d(group["pseudotime"], group[what_to_plot], "cubic", fill_value="extrapolate") #generate a function for every cell based on interpolation
    y = y_f(x) #calculate y for respective x value
    return y
    

# define function to interpolate all parameters of interest and save in dataframe
def interpolate_time_series(df, avg_cell_cycle):
    # generate numpy array for x-coordinates that are the new timepoints for averaging
    x = np.linspace(0,1,101)
    time_interval_avg_cell_cycle = round(avg_cell_cycle)
    x_avg_cell_cycle = x * time_interval_avg_cell_cycle

    #initiate empty list that will be filled successively with arrays
    lis_x = list() #will be appended with arrays containing the new x-values (always the same as defined above)
    lis_avg_cell_cycle_time = list() #list to store x values scaled to actual avg cell cycle duration
    lis_N_cell = list() #will be appended with arrays containing the new y-values as spit out by y_f(x), done so for all N, Con and VOl_mic3 values
    lis_N_nuc = list()
    lis_N_cyt = list()
    lis_con_cell = list()
    lis_con_nuc = list()
    lis_con_cyt = list()
    lis_vol_cell = list()
    lis_vol_nuc = list()
    lis_vol_cyt = list()
    
    cell_count = 1 #since a new df is being created and since it has a different size, the individual cells again need to be defined
    cell_counter_list = list()
    
    for name, group in df.groupby("Cell"): #iterate through time-series to interpolate individual parameters
        lis_x.append(x)
        lis_avg_cell_cycle_time.append(x_avg_cell_cycle)
        
        #interpolate time-series for all parameters:
        N_cell = interpolate (group, x, 'N_cell')
        N_nuc = interpolate (group, x, 'N_nuc')
        N_cyt = interpolate (group, x, 'N_cyt')
        Con_cell_nM = interpolate (group, x, 'Con_cell_nM')
        Con_nuc_nM = interpolate (group, x, 'Con_nuc_nM')
        Con_cyt_nM = interpolate (group, x, 'Con_cyt_nM')
        Vol_cell_mic3 = interpolate (group, x, 'Vol_cell_mic3')
        Vol_nuc_mic3 = interpolate (group, x, 'Vol_nuc_mic3')
        Vol_cyt_mic3 = interpolate (group, x, 'Vol_cyt_mic3')
        
        #add interpolated values to lists
        lis_N_cell.append(N_cell)
        lis_N_nuc.append(N_nuc)
        lis_N_cyt.append(N_cyt)
        lis_con_cell.append(Con_cell_nM)
        lis_con_nuc.append(Con_nuc_nM)
        lis_con_cyt.append(Con_cyt_nM)
        lis_vol_cell.append(Vol_cell_mic3)
        lis_vol_nuc.append(Vol_nuc_mic3)
        lis_vol_cyt.append(Vol_cyt_mic3)

        #add again a list with the respective identities (the old identities do not fit the new number of intervals)
        for i in range(len(x)):
            cell_counter_list.append(cell_count)
        cell_count += 1

    #put pseudotime data into new dataframe
    df_pseudotime = pd.DataFrame()
    df_pseudotime["pseudotime"] = np.concatenate(lis_x).ravel().tolist()
    df_pseudotime["avg_cell_cycle_time_hours"] = np.concatenate(lis_avg_cell_cycle_time).ravel().tolist()
    df_pseudotime['N_cell'] = np.concatenate(lis_N_cell).ravel().tolist()
    df_pseudotime['N_nuc'] = np.concatenate(lis_N_nuc).ravel().tolist()
    df_pseudotime['N_cyt'] = np.concatenate(lis_N_cyt).ravel().tolist()
    df_pseudotime['Con_cell_nM'] = np.concatenate(lis_con_cell).ravel().tolist()
    df_pseudotime['Con_nuc_nM'] = np.concatenate(lis_con_nuc).ravel().tolist()
    df_pseudotime['Con_cyt_nM'] = np.concatenate(lis_con_cyt).ravel().tolist()
    df_pseudotime['Vol_cell_mic3'] = np.concatenate(lis_vol_cell).ravel().tolist()
    df_pseudotime['Vol_nuc_mic3'] = np.concatenate(lis_vol_nuc).ravel().tolist()
    df_pseudotime['Vol_cyt_mic3'] = np.concatenate(lis_vol_cyt).ravel().tolist()
    df_pseudotime["Cell"] = cell_counter_list
    path = r'P:\Andi\FCS_cell_cycle\220307_Results\Extracted_parameters_combined_by_POI\04_interpolated_data'
    df_pseudotime.to_csv(os.path.join(path,df.name + ' interpolated.csv'))
    df_pseudotime.name = df.name
    return df_pseudotime


#################  NORMALIZATION BY MEXIT DATA ################################

def MExit_normalize_con_nuc(df, MExit_con_nuc):
    #get conversion factor between last (mitotic) Con_nuc of FCC 
    #and average mitotic Con_nuc from MEXit data
    df['diff'] = df.groupby('Cell')['Con_nuc_nM'].transform('last')/MExit_con_nuc
    #convert concentrations according to conversion factor
    df['Con_cell_nM_MExit'] = df['Con_cell_nM']/df['diff']
    df['Con_nuc_nM_MExit'] = df['Con_nuc_nM']/df['diff']
    df['Con_cyt_nM_MExit'] = df['Con_cyt_nM']/df['diff']
    #calculate new numbers based on new concentrations and the measured volumes
    NA = 0.602214086
    df["N_cell_MExit"] = df["Con_cell_nM_MExit"] * df["Vol_cell_mic3"] * NA
    df["N_nuc_MExit"] = df["Con_nuc_nM_MExit"] * df["Vol_nuc_mic3"] * NA
    df["N_cyt_MExit"] = df["Con_cyt_nM_MExit"] * df["Vol_cyt_mic3"] * NA
    return df

# function to normalize interpolated data to MExit N_cell
def MExit_normalize_N_cell(df, MExit_N_cell):
    #get conversion factor between last (mitotic) N_cell of FCC 
    #and average mitotic N_cell from MEXit data
    df['diff'] = df.groupby('Cell')['N_cell'].transform('last')/MExit_N_cell
    #convert numbers with that conversion factor
    df['N_cell_MExit'] = df['N_cell']/df['diff']
    df['N_nuc_MExit'] = df['N_nuc']/df['diff']
    df['N_cyt_MExit'] = df['N_cyt']/df['diff']
    #convert concentrations based on new numbers and known volumes
    NA = 0.602214086
    df["Con_cell_nM_MExit"] = df["N_cell_MExit"] / (df["Vol_cell_mic3"] * NA)
    df["Con_nuc_nM_MExit"] = df["N_nuc_MExit"] / (df["Vol_nuc_mic3"] * NA)
    df["Con_cyt_nM_MExit"] = df["N_cyt_MExit"] / (df["Vol_cyt_mic3"] * NA)
    return df


    ############ OVERVIEW PLOT ##################################################

    # function to plot all parameters N_cell, N_nuc...Vol_cell_mic3 of one POI in an overview plot with one subplot per parameter
def overview_plot_POI(df, color):
    #define axes
    fig, axs = plt.subplots(nrows=3, ncols=3,figsize=(12.5,6)) #((ax1,ax2,ax3), (ax4,ax5,ax6), (ax7,ax8,ax9))
    
    #define labels
    fig.text(0.5, 0.012, 'Time (hours)', ha='center', va='center', fontsize = 15)
    fig.text(0.0085, 0.8, 'Volume µm\u00b2', ha='center', va='center', rotation='vertical', fontsize = 15)
    fig.text(0.0085, 0.5, 'Concentration', ha='center', va='center', rotation='vertical', fontsize = 15)
    fig.text(0.0085, 0.2, 'N', ha='center', va='center', fontsize = 15)
    axs[0,0].set_title("Cell", fontsize = 15)
    axs[0,1].set_title("Nucleus", fontsize = 15)
    axs[0,2].set_title("Cytoplasm", fontsize = 15)
    
    #plot data
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Vol_cell_mic3', data = df, color = color, errorbar = "sd", ax = axs[0,0])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Vol_nuc_mic3', data = df, color = color, errorbar = "sd", ax = axs[0,1])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Vol_cyt_mic3', data = df, color = color, errorbar = "sd", ax = axs[0,2])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Con_cell_nM_MExit', data = df, color = color, errorbar = "sd", ax = axs[1,0])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Con_nuc_nM_MExit', data = df, color = color, errorbar = "sd", ax = axs[1,1])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'Con_cyt_nM_MExit', data = df, color = color, errorbar = "sd", ax = axs[1,2])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'N_cell_MExit', data = df, color = color, errorbar = "sd", ax = axs[2,0])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'N_nuc_MExit', data = df, color = color, errorbar = "sd", ax = axs[2,1])
    sns.lineplot(x = "avg_cell_cycle_time_hours", y= 'N_cyt_MExit', data = df, color = color, errorbar = "sd", ax = axs[2,2])
    
    #remove axis labels and set xticks - xticks is based on 18 avg cell cycle duration - has to be changed here in function for now!
    axs = axs.flatten()
    for ax in axs:
        ax.set_ylabel('')
        ax.set_xlabel('')
        ax.set_xticks([0,3,6,9,12,15,18])
    
    #set y axis limits for Volumes
    axs[0].set_ylim(0, round(max(df['Vol_cell_mic3'])))   
    axs[1].set_ylim(0, round(max(df['Vol_nuc_mic3'])))
    axs[2].set_ylim(0, round(max(df['Vol_cyt_mic3'])))
    #for ax in axs[0:3]:
    #    ax.set_ylim(0, round(max(df['Vol_cell_mic3'])))
    #set y axis limits for Concentrations    
    axs[3].set_ylim(0, round(max(df['Con_cell_nM_MExit'])))   
    axs[4].set_ylim(0, round(max(df['Con_nuc_nM_MExit'])))
    axs[5].set_ylim(0, round(max(df['Con_cyt_nM_MExit'])))
    #for ax in axs[3:6]:
    #    ax.set_ylim(0, round(max(df['Con_nuc_nM_MExit'])))
    #set y axis limits for Numbers  
    axs[6].set_ylim(0, round(max(df['N_cell_MExit']))) 
    axs[7].set_ylim(0, round(max(df['N_nuc_MExit'])))
    axs[8].set_ylim(0, round(max(df['N_cyt_MExit'])))

    # make layout such that subplots do not overlap
    plt.tight_layout()
    
    #save plot
    path = r"P:\Andi\FCS_cell_cycle\220307_Results\Plots"
    plt.savefig(os.path.join(path, df.name + ' overview.png'))