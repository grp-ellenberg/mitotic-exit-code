function [chrRegionCur1, chrRegionCur2, curCentX1, curCentY1, curCentZ1, curCentX2, curCentY2, curCentZ2, numCH] = ls_detect_chromosome_in_later_stacks(threeDLabel, stat, chrRegionPrev1, prevCentX1, prevCentY1, prevCentX2, prevCentY2, numPixels, numCH, voxelSizeX, voxelSizeY, voxelSizeZ)
%This is fuction detects chromosomes from the later time points
chrRegionCur1 = zeros(size(chrRegionPrev1));
chrRegionCur2 = zeros(size(chrRegionPrev1));
tempVol = zeros(size(chrRegionPrev1));
for i = 1: numel(stat)
    bDist = ((prevCentX1-stat(i).Centroid(2))^2 + (prevCentY1 - stat(i).Centroid(1))^2)^3;
    numPixels(i) = numPixels(i) * 1/bDist;
end
%Select the best - discard the one with artificial chromosome
chrRegionCur1(:,:,:) = 0;
[~,idx] = max(numPixels);
chrRegionCur1(threeDLabel.PixelIdxList{idx}) = 1;
numPixels(idx) = 0;

curCentX1 = stat(idx).Centroid(2);
curCentY1 = stat(idx).Centroid(1);
curCentZ1 = stat(idx).Centroid(3);
nc1Idx = idx;

if numCH == 1
    if sum(sum(sum(chrRegionCur1))) < sum(sum(sum(chrRegionPrev1)))*0.66
        tempVol(:,:,:) = 0;
        [~,idx] = max(numPixels);
        if idx ~= nc1Idx
            tempVol(threeDLabel.PixelIdxList{idx}) = 1;
            
            distPrevCh1 = sqrt((prevCentX1-curCentX1)^2 + (prevCentY1-curCentY1)^2);
            distPrevCh2 = sqrt((prevCentX1-stat(idx).Centroid(2))^2 + (prevCentY1-stat(idx).Centroid(1))^2);
            vol1 = sum(sum(sum(chrRegionCur1)));
            vol2 = sum(sum(sum(tempVol)));
            totalVol = vol1 + vol2;
            %We dont want to detect cell division for this analysis, the
            %below is commented out
%             if totalVol <= sum(sum(sum(chrRegionPrev1)))*1.1 && distPrevCh1 <= distPrevCh2*3 && distPrevCh2 <= distPrevCh1*3 && vol1 >= vol2*0.6 && vol2 >= vol1*0.6
%                 chrRegionCur2(threeDLabel.PixelIdxList{idx}) = 1;
%                 curCentX2 = stat(idx).Centroid(2);
%                 curCentY2 = stat(idx).Centroid(1);
%                 curCentZ2 = stat(idx).Centroid(3);
%                 numCH = 2;
%             end
        end
    end
else
    if sum(sum(sum(chrRegionCur1)))> sum(sum(sum(chrRegionPrev1)))*1.85
        [chrRegionCur1, chrRegionCur2] = ls_split_nucleus_ana_telo(chrRegionCur1,voxelSizeX, voxelSizeY, voxelSizeZ);
        
        linIndex = find(chrRegionCur1(:,:,:) > 0);
        [xCoords, yCoords, zCoords] = ind2sub(size(chrRegionCur1),linIndex);
        curCentX1 = sum(xCoords)/length(xCoords);
        curCentY1 = sum(yCoords)/length(yCoords);
        curCentZ1 = sum(zCoords)/length(zCoords);
        clear xCoords;
        clear yCoords;
        clear zCoords;
        
        linIndex = find(chrRegionCur2(:,:,:) > 0);
        [xCoords, yCoords, zCoords] = ind2sub(size(chrRegionCur2),linIndex);
        curCentX2 = sum(xCoords)/length(xCoords);
        curCentY2 = sum(yCoords)/length(yCoords);
        curCentZ2 = sum(zCoords)/length(zCoords);
        clear xCoords;
        clear yCoords;
        clear zCoords;
        
        distPrevCh1 = sqrt((prevCentX1-curCentX1)^2 + (prevCentY1-curCentY1)^2);
        distPrevCh2 = sqrt((prevCentX1-curCentX2)^2 + (prevCentY1-curCentY2)^2);
        
        if distPrevCh2<distPrevCh1
            tempX = curCentX1;
            tempY = curCentY1;
            tempZ = curCentZ1;
            curCentX1 = curCentX2;
            curCentY1 = curCentY2;
            curCentZ1 = curCentZ2;
            
            curCentX2 = tempX;
            curCentY2 = tempY;
            curCentZ2 = tempZ;
            tempVol = chrRegionCur1;
            chrRegionCur1 = chrRegionCur2;
            chrRegionCur2 = tempVol;
        end
    else
        numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
        for i = 1: numel(stat)
            if i == nc1Idx
                numPixels(i) = 0;
            end
            bDist = ((prevCentX2-stat(i).Centroid(2))^2 + (prevCentY2 - stat(i).Centroid(1))^2)^3;
            numPixels(i) = numPixels(i) * 1/bDist;
        end
        %Select the best - discard the one with artificial chromosome
        chrRegionCur2(:,:,:) = 0;
        [~,idx] = max(numPixels);
        chrRegionCur2(threeDLabel.PixelIdxList{idx}) = 1;
        %numPixels(idx) = 0;
        
        curCentX2 = stat(idx).Centroid(2);
        curCentY2 = stat(idx).Centroid(1);
        curCentZ2 = stat(idx).Centroid(3);
    end
end

if numCH == 1
    curCentX2 = curCentX1;
    curCentY2 = curCentY1;
    curCentZ2 = curCentZ1;
end
end

