function [stackHom] = ls_inhomogeneous_to_homogeneous(stack)
%Replace very bright intensities with average
[threshold, ~] = ls_otsu_3d_image(stack, 0);
%histogram(1:threshold) = 0;

%[threshold] = Otsu_3D_Hist(histogram,length(histogram), 0);
stackHom = stack;
%This condition is to be set with more suitable value
if sum(sum(sum(stack>threshold/2)))>sum(sum(sum(stack>threshold)))*2
    stackHom(stack(:,:,:)>threshold) = threshold;
end
end