function [reader, dim, timeStamps] = getBioformatsReader(filepath, frameInt, voxelSize)
% getBioformatsReader(filepath, frameInt, voxelSize)
%   return a bioformat's reader, dimensions of object, and timeStamps from
%   a multitif file
%   INPUT:
%       filepath - filepath of multitif file
%       frameInt - frame to be used to extract the timeStamps can be a
%       vector (optional)
%       voxelSize - impose voxelSize in um (optional)
%   OUTPUT: 
%       reader - a bioformat reader object
%       dim    - dimension of object struct('Nx', Nx, 'Ny', Ny, 'Nz', Nz, 'Nc', Nc, 'Nt', Nt, 'voxelSize', voxelSize);
%       timeStamps - time stamps extracted from file (if available)
% To use bioformats: install https://www.openmicroscopy.org/site/support/bio-formats5.3/users/matlab/
% Antonio Politi, EMBL, November 2016
    
   
    assert(exist(filepath, 'file') > 0, [filepath ' does not exists'])
    if nargin < 2
        frameInt = 1;
    end
    if nargin < 3
        voxelSize = [];
    end
    if ~exist(filepath)
        error(sprintf('%s is not a file', filepath));
    end
    try
        reader = bfGetReader(filepath);
    catch
        error('This program recquires java library for bioformats. See http://downloads.openmicroscopy.org/bio-formats/5.0.0/')
    end
    %get Metadata access class
    omeMeta = reader.getMetadataStore();
    Nx = omeMeta.getPixelsSizeX(0).getValue();
    Ny = omeMeta.getPixelsSizeY(0).getValue();
    Nz = omeMeta.getPixelsSizeZ(0).getValue();
    Nc = omeMeta.getPixelsSizeC(0).getValue();
    Nt = omeMeta.getPixelsSizeT(0).getValue();
    if isempty(voxelSize)
        try
            voxelSize = [omeMeta.getPixelsPhysicalSizeX(0).value; omeMeta.getPixelsPhysicalSizeY(0).value; omeMeta.getPixelsPhysicalSizeZ(0).value];
        catch
            warning('getBioformatsReader; Was not able to read size pixelSize Z')
            voxelSize = [double(omeMeta.getPixelsPhysicalSizeX(0).value); double(omeMeta.getPixelsPhysicalSizeY(0).value);  1];
        end
    end
    voxelSize = double(voxelSize);
    dim = struct('Nx', Nx, 'Ny', Ny, 'Nz', Nz, 'Nc', Nc, 'Nt', Nt, 'voxelSize', voxelSize);
    timeStamps = zeros(length(frameInt), 2);
    if Nt == 1
        timeStamps = [];
        return
    end
    try
        for i = 1:length(frameInt)
            timeStamps(i,:) = [frameInt(i) double(omeMeta.getPlaneDeltaT(0,(frameInt(i)-1)*dim.Nc*dim.Nz + 1))];
        end
    catch
        try
            assert(exist(fullfile(fileparts(filepath),'timeStamps.txt'), 'file')>0, 'No time metadata could be retrieved. Please generate a file timeStamps.txt that contains the time for each frame');
            warning('Cannot retrieve metadata for time Stamps program will use file timeStamps.txt');
            %try to read from file
            time = load(fullfile(fileparts(filepath),'timeStamps.txt'));
            timeStamps = [frameInt' time(frameInt)];
        catch
            warning('getBioformatsReader: was not able to detect time stamp from file');
        end
    end
end