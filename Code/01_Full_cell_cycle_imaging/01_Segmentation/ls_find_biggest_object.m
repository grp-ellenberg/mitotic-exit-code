function biggestobject=findbiggestobject_bwconcomp(image,connection)
%This function returns biggest object in input image
biggestobject = image;
twoDLabel = bwconncomp(image,connection);
if twoDLabel.NumObjects > 0
    numPixels = cellfun(@numel,twoDLabel.PixelIdxList);
    [~, idx] = max(numPixels);
    biggestobject = image;
    biggestobject(:,:) = 0;
    biggestobject(twoDLabel.PixelIdxList{idx}) = 1;
end

