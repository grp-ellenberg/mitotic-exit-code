function [outStack, x_offset, y_offset] = fn_crop_stack(inStack,cropDx, cropDy, centX, centY, nRemoveLowerSlices)
% This function crops inStack from image centre in xy and removes nRemoveLowerSlices 
% number slice from the lower part of the stack  
centX = round(centX);
centY = round(centY);

[inDx, inDy, inDz] = size(inStack);

if (cropDx >= inDx || cropDy >= inDy) && nRemoveLowerSlices ==0
    outStack = inStack;
    return;
end

outDz = inDz - nRemoveLowerSlices;
outStack = zeros(cropDx, cropDy, outDz);
if cropDx < inDx && cropDy < inDy

    %centX = round(inDx/2);
    %centY = round(inDy/2);

    halfCropX = round(cropDx/2);
    halfCropY = round(cropDy/2);
    
    %In case cropping does not have enough pixels from image border
    if centX< halfCropX+1
        centX = halfCropX+1;
    end    
    if centY< halfCropY+1
        centY = halfCropY+1;
    end

    if centX > inDx - halfCropX
        centX = inDx - halfCropX;
    end
    
    if centY > inDy - halfCropY
        centY = inDy - halfCropY;
    end
        
    for zplane = 1: outDz
        outStack(:,:,zplane) = inStack(centX-halfCropX:centX+halfCropX-1, centY-halfCropY:centY+halfCropY-1, zplane + nRemoveLowerSlices);    
    end
else
    for zplane = 1: outDz
        outStack(:,:,zplane) = inStack(:, :, zplane + nRemoveLowerSlices);    
    end
end
x_offset = centX-halfCropX;
y_offset = centY-halfCropY;
end

