function [detLabel] = fn_get_ws_label_on_chromosome(wsLabel,chromosome, chrLabel)
%Get watershed label on chromosome region with chrLabel value
wsLabel(chromosome ~= chrLabel) = 0;
maxLabel = max(wsLabel(:));

detLabel = 1;
maxValue = sum(sum(sum(wsLabel == 1)));

for i=1:maxLabel
   curValue = sum(sum(sum(wsLabel == i)));
   if curValue >maxValue
       maxValue = curValue;
       detLabel = i;
   end
end

