# 01 Full Cell Cycle Imaging

## Segmentation
The segmentation pipeline employed in Brunner et al. 2024 is based on [Cai et al. 2018](https://www.nature.com/articles/s41586-018-0518-z) and [Cattoglio et al. 2018](https://elifesciences.org/articles/40164) and requires MTALAB.

### Requirements

**Toolboxes:** Computer Vision Toolbox, Curve Fitting Toolbox\
**Image and segmentation parameters** are to be defined in fn_running_options.m:
* image directory, following acquisition folder structure of MyPic
* output directory
* image file extension (.czi or .tif)
* segmentation parameters, especially analyzed cropped region

### Segmented data (.csv files) are provided [here](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/01_Full_cell_cycle_imaging/01_time-series_data?ref_type=heads)

## Analysis

Contains [functions for analysis](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/01_Full_cell_cycle_imaging/02_Analysis/FCC_functions.py?ref_type=heads) and a [script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/01_Full_cell_cycle_imaging/02_Analysis/FCC_analysis.ipynb?ref_type=heads) for the combined analysis and plotting of the segmented data plotting of full cell cycle data.

Uses [FCS-calibrated data from the mitotic exit dataset](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Data/01_Full_cell_cycle_imaging/02_MExit_normalization?ref_type=heads) to calibrate time-series data.
