# 07 Code for Analysis of STED imaging data

## Preparation of raw STED data
Use [Fiji macro](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/tree/main/Code/07_STED/01_Segmentation?ref_type=heads) to 1) background-subtract, 2) segment and 3) save .tif image files in a new directory.

## Analysis of STED data
STED analysis was performed using custom written Python code:
* STED analysis functions are stored in separate [script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/07_STED/02_Analysis/STED_analysis_functions.py?ref_type=heads) which is loaded in analysis notebooks
* Main [Analysis Notebook](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/07_STED/02_Analysis/STED_analysis_replicate_data.ipynb?ref_type=heads) performs data loading, STED spot segmentation and analysis for a given replicate
* [Co-localization analysis script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/07_STED/02_Analysis/STED_Pearson_analysis_combined.ipynb?ref_type=heads) loads image data and performs Pearson's correlation coefficient analysis of Cohesin-STAG and CTCF channels. Resulting data is plotted and statistically analyzed
* [Spot simulation script](https://git.embl.de/grp-ellenberg/mitotic-exit-code/-/blob/main/Code/07_STED/02_Analysis/STED_random_spot_simulation.ipynb?ref_type=heads) was used to simulate STED spot images to inform analysis of experimental data. 
  

