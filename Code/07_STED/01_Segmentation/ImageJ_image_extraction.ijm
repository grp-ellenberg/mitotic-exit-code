// because the AF594 confocal images had cytosolic background, I chose ch1 for nucleus segmentation. For that I had to import the confocal images first (=this is different to replicate 1 and 2 image export).

run("Close All");

//specify paths etc
condition = "eG1_STAG1"
input_folder = "P:/Andi/STED/2024-01-24_STED_Replicate3_AB/"
image = "CL009_Ibidi1_ChD_eG1_STAG1-EGFP-635P_CTCF-AF594.lif"
output_folder = "P:/Andi/STED/2024-01-25_Replicate3_analysis/"
Series_time_gated = "1"
Series_tau_STED = "12"
//Series_time_gated = "15"
//Series_tau_STED = "26"
//Series_time_gated = "29"
//Series_tau_STED = "40"
//Series_time_gated = "43"
//Series_tau_STED = "54"
//Series_time_gated = "57"
//Series_tau_STED = "68"
//Series_time_gated = "71"
//Series_tau_STED = "82"

//load confocal image C1 via time-gated STED stack
run("Bio-Formats Importer", "open=" + input_folder + image + " autoscale color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_"+ Series_time_gated); 
image_name = getTitle();

image_path_list = split(image_name,"/"); //#splits the image where "/" is. Based on path, this varies in length
image_name_short = image_path_list[image_path_list.length - 3];
output_path = output_folder + condition + "/" + image_name_short;
//print(image_name, image_name_short)
//print(output_path);

run("Split Channels");
selectWindow("C1-" + image_name);

// segment nucleus based on confocal image
run("Duplicate...", " ");
run("Gaussian Blur...", "sigma=25");
run("Convert to Mask");
run("Analyze Particles...", "size=40-Infinity clear include add");
// get single ROI and save mask
roiManager("Select", 0);
run("Duplicate...", " ");
run("Clear Outside");
saveAs("Tiff", output_path + "_mask.tif");
close();
// also close masked C1
selectWindow("C1-" + image_name+"-1");
close();
//// close confocal image
selectWindow("C1-" + image_name);
roiManager("Select", 0);
run("Duplicate...", " ");
saveAs("Tiff", output_path + "_confocal_C1.tif");

// save C3 image
selectWindow("C3-" + image_name);
roiManager("Select", 0);
run("Duplicate...", " ");
saveAs("Tiff", output_path + "_confocal_C3.tif");
run("Close All");

// save tauSTED and C3 confocal images
run("Bio-Formats Importer", "open=" + input_folder + image + " autoscale color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_"+ Series_tau_STED); image_name = getTitle();
// split image channels, subtract background only for C2, C4
run("Split Channels");
//close empty C1 image (will be saved later from time-gated image)
selectWindow("C1-" + image_name);
close();

//subtract background for C2, C4
selectWindow("C2-" + image_name);
run("Subtract Background...", "rolling=50 stack");
selectWindow("C4-" + image_name);
run("Subtract Background...", "rolling=50 stack");

//// generate co-loc image and save
run("Merge Channels...", "c2=[C4-" + image_name +  "] c6=[C2-"+ image_name + "] create keep");
selectWindow("Composite");
roiManager("Select", 0);
run("Duplicate...", "duplicate");
saveAs("Tiff", output_path + "_composite.tif");
close();
selectWindow("Composite");
saveAs("Tiff", output_path + "_composite_full.tif");
close();

// generate crops of STED signal in nucleus for Pearson correlation analysis
selectWindow("C2-" + image_name);
roiManager("Select", 0);
run("Duplicate...", " ");
run("Clear Outside");
saveAs("Tiff", output_path + "_C2_tauSTED_nuc.tif");
selectWindow("C2-" + image_name);
close();
selectWindow("C4-" + image_name);
roiManager("Select", 0);
run("Duplicate...", " ");
run("Clear Outside");
saveAs("Tiff", output_path + "_C4_tauSTED_nuc.tif");
run("Close All");


