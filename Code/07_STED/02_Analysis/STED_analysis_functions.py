### File containing functions for STED analysis
### Date: 09.01.2024
### Author: Andreas Brunner
### Description: Collection of functions for the segmentation of STED images, including fine segmentation, spot detection and spot intensity measurements

import numpy as np 
import tifffile as tf
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_mean, threshold_triangle, gaussian
from skimage.segmentation import watershed
from skimage.feature import peak_local_max
from skimage.morphology import binary_erosion, binary_dilation
from skimage.morphology import area_opening
from skimage.measure import label
from scipy import ndimage as ndi
from tqdm import tqdm
import pandas as pd
from skimage.measure import regionprops_table
import seaborn as sns
import numpy.ma as ma


def image_import(folder_path, image_name, number_of_images, start = 1):
    """
    Function to import STED images
    The "start" variable was implemented as Series001 of STAG2_G1 in Replicate 3 had to be removed for quality reasons. Set to 1 as default to load all image series.
    """
    image_list = list()
    #import images and add to list
    for image_number in range(start, number_of_images+1, 1): #iterate through specified number of images
        Series_list = list()
        C1_image = tf.imread(folder_path + "\\" +image_name + " - Series00" + str(image_number) + "_confocal_C1.tif") #load images
        C2_image = tf.imread(folder_path + "\\" +image_name + " - Series00" + str(image_number) + "_C2_tauSTED_nuc.tif")
        C3_image = tf.imread(folder_path + "\\" +image_name + " - Series00" + str(image_number) + "_confocal_C3.tif")
        C4_image = tf.imread(folder_path + "\\" +image_name + " - Series00" + str(image_number) + "_C4_tauSTED_nuc.tif")
        mask = tf.imread(folder_path + "\\" +image_name + " - Series00" + str(image_number) + "_mask.tif")
        Series_list.append(C1_image) #append to list. This ensures that all images of a single Series are in one list and can be accessed at the same time.
        Series_list.append(C2_image)
        Series_list.append(C3_image)
        Series_list.append(C4_image)
        Series_list.append(mask)
        image_list.append(Series_list)
        print(folder_path + "\\" +image_name + " - Series00" + str(image_number) + " loaded")
    return image_list

def coarse_segment(image, erosion = True, dilation = True, area_threshold = 3, threshold_fact = 1.3, min_thresh = None):
    """
    Function to coarsely segment STED images.
    Uses gaussian blurred background-corrected tau-STED images to generate a binary mask. This mask is eroded, filtered and dilated to remove tiny spots.
    Finally, binary mask is used to create primary spot labels. These labels will be refined in refine_segment function.
    """
    image[image < 0] = 0                                                # replaces negative values in image with 0 
    gauss_filtered = gaussian(image, sigma = 1, preserve_range = True)  # minor gaussian filter to enable bit smoother segmentation
    thresh = threshold_otsu(gauss_filtered)                             # threshold image.
    if min_thresh != None:                                              # if no proper threshold can be identified, it can be set manually
        if min_thresh > thresh:
            thresh = min_thresh
            print("Use minimum treshold = " + str(thresh))
        else:
            pass
    else:
        pass
    thresh = thresh * threshold_fact                                    # enhance trheshold to be more restrictive
    binary = gauss_filtered >= thresh                                   # make binary based on threshold
    
    if erosion == True:                                                 # erode to remove small spots and define outline of actual spots better
        eroded = binary_erosion(binary)
    else:
        eroded = binary
    
    filtered = area_opening(eroded, area_threshold=area_threshold, connectivity=1) # filter out small spots
    
    if dilation == True:                                                # dilate spots to represent their actual size again
        dilated = binary_dilation(filtered)
    else:
        dilated = filtered
    
    label_image = label(dilated)                                        # create labels for every spot
    print("Coarse segmentation DONE, threshold = " + str(thresh))
    return gauss_filtered, thresh, binary, eroded, filtered, dilated, label_image # returns gauss filtered image, threshold value, binary image, eroded and filtered image, as well as primary spot labels. Those will be used to refine the spot segmentation.


def refine_segment(image, label_image):
    """
    Function to refine segmentation of coarsely segmented STED image spots.
    Requires STED image and labels (label_image, exported from coarse_segment function)
    """
    spot_labels = np.zeros_like(image)                               # initiae 2D array of image size in which refined spot labels will be stored collectively (= will be updated label_image)  
    spot_labels = spot_labels.astype(int)                            # convert zeros to float
    label_counter = 0                                                # initiate label counter
    print("START refined segmentation.")

    labels_list = []                                                 # we will iterate through coarsely segmented spots and generate a new array with refined labels. Those will be saved temporarily in the labels_list.
    for label_id in tqdm(range(1, label_image.max())):               # omit label 0 as this is background.
        spot_mask = (label_image == label_id)                        # generate a boolean spot mask of the spot of interest
        spot_image = np.zeros_like(image)                            # extract pixel values from actual image, first initiate array of image size filled with zeros
        spot_image = np.where(spot_mask, image, 0)                   # Apply the mask to the pixel values array
        spot_image = spot_image.astype(int)                          # convert spot_image values to int
        spot_image = np.where(spot_image > 0, spot_image, 0)         # again mask the spot_image based on the pixels that have values >0. For some reason this is required for some images. Peak_local_max otherwise detects peaks outside the mask.

        coords = peak_local_max(spot_image, min_distance=5, num_peaks=100) # detect local peaks in coarsely segmented psot. Min distance and dilation have a major influence on spot density.

        mask = np.zeros(spot_image.shape, dtype=bool)                # initiate mask array with boolean values
        mask[tuple(coords.T)] = True                                 # transpose coordinates and set peak coordinates to True
        markers, _ = ndi.label(mask)                                 # label peaks with numbers
        labels = watershed(-spot_image, markers, mask=spot_image)    # performs watershed of peaks within mask
        labels[labels != 0] += label_counter                         # all labeled pixels with label higher than 0 (which is background) are incremented by the label_counter value. The label_counter value is the highest value of the last previously added refined spot label.
        labels_list.append(labels)                                   # append labels_list with current new labels from the current coarsely segmented spot.
        label_counter = labels.max()                                 # sets the current maximum labels number as the current label_counter
    spot_labels = np.sum(labels_list, axis=0)                        # sums up all labels within the individual arrays stored in labels_list into one array called spot_labels. This is the refined label_image.
    print("MAX spot_labels number: " + str(len(np.unique(spot_labels[spot_labels !=0]))))
    return spot_labels

def extract_spot_int(image, labels, channel):
    """
    Function to refine extract intensity of refined segmented STED image spots.
    Requires STED image and labels (spot_labels exported from refined_segment).
    """
    # measure the voxel counts
    measurements_table = regionprops_table(label_image = labels, intensity_image = image, properties = ['label','area', 'intensity_mean', 'intensity_min', 'intensity_max'])
    # convert to a dataframe
    measurements_df = pd.DataFrame(measurements_table)
    measurements_df_cleaned = measurements_df[measurements_df.area >5] # spots smaller than 5 pixels are removed
    measurements_df_removed = measurements_df[measurements_df.area <5] #uncomment to check whether sum of spots is correct
    if channel == "C2":
        print("Channel 1 & 2: Number of spots passed QC: ", len(measurements_df_cleaned), " Number of small spots removed: ", len(measurements_df_removed))
    elif channel == "C4":
        print("Channel 3 & 4: Number of spots passed QC: ", len(measurements_df_cleaned), " Number of small spots removed: ", len(measurements_df_removed))
    else:
        pass
    return measurements_df_cleaned #, measurements_df_removed

def area(mask_img):
    """
    Function to measure mask area.
    """
    label_image = label(mask_img)                                    #create label image from mask                      
    measurements_table = regionprops_table(label_image = label_image, intensity_image = mask_img, properties = ['area', 'intensity_mean']) # measure area measurement in pixel counts
    area = measurements_table["area"][0]                             # extract area measuremen
    return area

### SUMMARY AND SPOT ANALYSIS FUNCTION FOR BOTH tau-STED AND CONFOCAL IMAGES
def spots_analysis_confocal_and_STED(C1, C2, C3, C4, mask, threshold_fact, min_thresh = None):
    """
    Function to perform coarse and fine segmentation of selected STED image in all 4 channels
    C1 = confocal channel STAG-GFP
    C2 = STED channel STAG-GFP
    C3 = confocal channel CTCF
    C4 = STED channel CTCF
    mask image
    threshold_fact to be manually specified. This is based on prior segmentation tests.
    """
    # Perform coarse and refined segmentation on STAG channels
    C2_gauss_filtered, C2_thresh, C2_binary, C2_eroded, C2_filtered, C2_dilated, C2_label_image = coarse_segment(C2, erosion = True, dilation = True, area_threshold = 3, threshold_fact = threshold_fact, min_thresh = min_thresh)
    C2_spot_labels = refine_segment(C2, C2_label_image)
    # Perform coarse and refined segmentation on CTCF channels
    C4_gauss_filtered, C4_thresh, C4_binary, C4_eroded, C4_filtered, C4_dilated, C4_label_image = coarse_segment(C4, erosion = True, dilation = True, area_threshold = 3, threshold_fact = threshold_fact, min_thresh = min_thresh)
    C4_spot_labels = refine_segment(C4, C4_label_image)
    # Measure spot intensities in each channel based on detected spots in both channel. These measurements are output as dataframes
    C1_spot_measurements = extract_spot_int(C1, C2_spot_labels, "C1")
    C2_spot_measurements = extract_spot_int(C2, C2_spot_labels, "C2")
    C3_spot_measurements = extract_spot_int(C3, C4_spot_labels, "C3")
    C4_spot_measurements = extract_spot_int(C4, C4_spot_labels, "C4")
    area_mask = area(mask) # to collect measure of overall nucleus area.
    return C1_spot_measurements, C2_spot_measurements, C3_spot_measurements, C4_spot_measurements, area_mask

def spot_summary_confocal_and_STED(image_list, threshold_fact = 1.1, min_thresh = None):
    """
    Function to perform spot analysis on all images of a list of images
    """
    df_C1 = pd.DataFrame()                                          # initiate empty dataframe to store multiple dataframe (one for each series) inside them
    df_C2 = pd.DataFrame()
    df_C3 = pd.DataFrame()
    df_C4 = pd.DataFrame()
    list_C1 = list()                                                # initiate list to collect C1, C2... measurement dataframes for each individual series (=image)
    list_C2 = list()
    list_C3 = list()
    list_C4 = list()
    for image_series in tqdm(range(0, len(image_list))):            # iterate through series to segment spots and make measurements
        C1_spot_measurements, C2_spot_measurements, C3_spot_measurements, C4_spot_measurements, area_mask = spots_analysis_confocal_and_STED(image_list[image_series][0], 
                                                                                                                                             image_list[image_series][1], 
                                                                                                                                             image_list[image_series][2], 
                                                                                                                                             image_list[image_series][3], 
                                                                                                                                             image_list[image_series][4], threshold_fact = threshold_fact, min_thresh = min_thresh)
        C1_spot_measurements["image"] = image_series                # append the individual measurment dataframes by image-id and nuclear area
        C2_spot_measurements["image"] = image_series
        C3_spot_measurements["image"] = image_series
        C4_spot_measurements["image"] = image_series
        C1_spot_measurements["area_mask"] = area_mask
        C2_spot_measurements["area_mask"] = area_mask
        C3_spot_measurements["area_mask"] = area_mask
        C4_spot_measurements["area_mask"] = area_mask
        list_C1.append(C1_spot_measurements)                        # append dataframes to list
        list_C2.append(C2_spot_measurements)
        list_C3.append(C3_spot_measurements)
        list_C4.append(C4_spot_measurements)
    df_C1 = pd.concat(list_C1)                                      # concat all collected measurement dataframes to one final df for each channel
    df_C2 = pd.concat(list_C2)
    df_C3 = pd.concat(list_C3)
    df_C4 = pd.concat(list_C4)
    return df_C1, df_C2, df_C3, df_C4

##########################################################################################
### SPOT INTENSITY HISTOGRAMS
##########################################################################################
def compare_spot_intensity(df1, df2, color1, color2, save = False, path = None, xlim_max = 70):
    """
    Function to compare spot intensities between two conditions (POI/cell cycle stage)
    """
    fig, ax = plt.subplots(1,1, figsize = (4,3))
    plt.hist(x = df1["intensity_mean"], bins = 40, density = True, alpha = 0.5, color = color1)
    plt.hist(x = df2["intensity_mean"], bins = 40, density = True, alpha = 0.5, color = color2)
    print("Mean spot intensity of df1 (color = " + color1 + "): \t" + str(round(np.mean(df1["intensity_mean"]),2)) + ",\tmedian spot intensity: " + str(round(np.median(df1["intensity_mean"]),2)))
    print("Mean spot intensity of df2 (color = " + color2 + "): \t" +str(round(np.mean(df2["intensity_mean"]),2)) + ",\tmedian spot intensity: " + str(round(np.median(df2["intensity_mean"]),2)))
    plt.xlim(0,xlim_max)
    if save == True:
        plt.savefig(path, dpi=300, format="pdf", transparent=True)
    else:
        plt.show()

###########################################################################################
### Functions to compute and plot Pearsons correlation coefficient
###########################################################################################

def Pearson(image_list):
    """
    Function to compute Pearson's correlation coefficient between CTCF and Cohesin-STAG1/2 channels
    """
    corr_list = list()
    for image_no in range(0, len(image_list)):
        image_C2 = np.where((image_list[image_no][4]/255).astype(bool), image_list[image_no][1], np.nan) #using values inside mask was important because otherwise corr coeff would be heavily influenced by pixels outside mask
        image_C4 = np.where((image_list[image_no][4]/255).astype(bool), image_list[image_no][3], np.nan)
        correlation = ma.corrcoef(ma.masked_invalid(image_C2.flatten()), ma.masked_invalid(image_C4.flatten()))[1,0] #np.corrcoeff does not work with nan values, ma.corrcoeff with ma.masked_invalid does
        corr_list.append(correlation)
        # print(correlation)
    return corr_list

# to plot results as boxplot
def boxplot(df, color):
    """
    Function to plot Pearson's correlation coefficient results (every spot = 1 cell)
    """
    fig, ax = plt.subplots()
    fig.set_figheight(4)
    fig.set_figwidth(2.5)
    sns.scatterplot(x=df["stage"], y = df["correlation"], color = color)
    ax = sns.boxplot(x=df["stage"], y = df["correlation"], showfliers = False, color = color)
    plt.xlabel("")
    ax.set_xticklabels(["early G1", "G1"], rotation=60, ha='right', fontsize = 15)
    ax.set_ylim(0,0.25)
    plt.ylabel("Pearson Correlation", fontsize = 15)
    plt.yticks(fontsize = 12)